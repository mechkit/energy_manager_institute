## Domain I: Application 15% 

### Task 1: Describe types of PV system applications:
a: Grid-interactive systems with and  without storage
b. Stand-alone systems for residential,  commercial, and industrial applications
c. Remote industrial systems
d. Specialty applications
e. Solar-integrated products

### Task 2: Identify key features and benefits of specific types of PV systems:
a. Energy security
b. Predictable electricity costs
c. Simplicity of design and installation
d. Environmental impact and social benefit
e. Economic benefits
f. Portability of system
g. System cost
h. Reliability of performance

### Task 3: List the key components of specific types of PV systems:
a. Modules
b. Structural attachments (e.g., racking, mounting)
c. Power electronics (e.g., inverters, optimizers, charge controllers)
d. Switch gear
e. Balance of system components
f. Point of utility interconnection
g. Energy storage
h. Monitoring equipment

### Task 4: Understand the safety concerns associated with the different types of PV systems:
a. Fall hazards
b. DC hazards (e.g., electrical arcing, fire)
c. AC hazards (e.g., arc flash)
d. Shock hazards
e. Environmental and jobsite hazards
f. Lifting
g. Hazardous materials

### Task 5: List the advantages and disadvantages of PV system compared to other electricity generation sources:
a. Economics
b. Accessibility to the site
c. Reliability of the system
d. MaintenanceCATEGORIZATION OF 
e. Environmental impact
f. Efficiency
g. Distributed generation

## Domain II: Sales and Economics 13% 

### Task 1: Determine necessary customer information to collectKnowledge of:
a. Physical location
b. Credit strength (e.g., FICO score)
c. Utility information
d. Site information (e.g., shading factors, roof type, orientation, pitch, electrical service, local jurisdictions)

### Task 2: Identify the customer’s motivations to install solarKnowledge of:
a. Financial reasons
b. Environmental concerns
c. Energy independence
d. Energy security

### Task 3: Estimate system size to meet the customer’s financial objectiveKnowledge of:
a. Customer energy usage
b. Utility rate structure
c. Available incentives
d. Customer budget
e. General system sizing (e.g., calculations)

### Task 4: Identify information from a client customer utility bill relevant to grid‐interactive solarKnowledge of:
a. Existing rate schedule and options
b. Customer usage profile (e.g., daily patterns, seasonal patterns)
c. Demand charges (e.g., peak load)

### Task 5: Identify information from the client on electricity usage relevant to stand‐alone solarKnowledge of:
a. Power and energy requirements (e.g., critical load)
b. Customer usage profile (e.g., daily patterns, seasonal patterns)
c. Days of autonomy
e. Technology
f. Aesthetics
g. Health
h. Status

### Task 6: List key factors that impact the economics of solarKnowledge of:
a. Incentives
b. Net energy metering
c. Environmental conditions
d. System price (e.g., equipment, labor, soft costs)
e. Cost of electricity from utility

### Task 7: Recognize how policies and available financial benefits affect different PV marketsKnowledge of:
a. PV markets (e.g., residential, commercial, non-profit, utility scale PV)
b. Local policies (e.g., tax credits, local benefits, utility benefits, net energy metering, zero net export)

### Task 8: Identify financial risks associated with PV systemsKnowledge of:
a. Electricity rate fluctuation
b. System performance
c. Policy uncertainty
d. Warranty limitations (e.g., existing roof, PV system products, workmanship)
e. Maintenance costs

### Task 9: Identify common financing options, knowledge of:
a. Cash purchase
b. Lease
c. Power purchase agreement (PPA)
d. Loan (e.g., Property Assessed Clean Energy [PACE], line of credit, home equity)

### Task 10: Identify predictable maintenance costs over the life of the systemKnowledge of:
a. Maintenance and equipment replacement costs (e.g., mechanical, batteries, inverters, service contracts)
b. Cleaning costs
c. Monitoring costs
d. Roofing replacement
f. Irradiation
g. Energy storage
h. Cost to finance
i. Equipment degradation (e.g., modules)
j. Operation and maintenanceDOMAIN II: SALES AND ECONOMICS



## Domain III: Design 25%

### Task 1: Ensure equipment is appropriate for intended useKnowledge of:
a. National recognized testing labs (e.g., Underwriters Laboratories, National Energy Testing Laboratory)
b. Product safety standards (e.g., IEEE standards, national standards)
c. Manufacturer instructions
d. Location conditions (e.g., wind, snow, seismic)
e. Electrical hazards

### Task 2: Identify relevant codes and requirements that impact PV design and installationKnowledge of:
a. Electrical codes
b. Building codes
c. Fire safety codes
d. Workplace safety codes
e. Local permitting requirements
f. Local utility requirements 

### Task 3: Recognize electrical concepts and terminologyKnowledge of:
a. Ohm’s law
b. Power and energy
c. Electrical measurements (e.g., voltage, current, impedance, resistance)
d. Alternating current (AC) and direct current (DC)
e. Single-phase, split-phase, and three-phase circuits
f. Series and parallel circuits
g. Properties of common conductors (e.g., insulation types [PV wire, USE-2, THWN-2], sizes and ampacities, voltage ratings, color)
h. Types of common raceways (e.g., EMT, RMC, PVC, LFNC, LFMC, FMC)
i. Types of common multi-conductor cables (e.g., Romex, MC, TC)
j. Grounding and bonding terminology (e.g., equipment grounding conductor, grounding electrode conductor, grounding electrode, bonding jumper)

### Task 4: Identify factors impacting solar resource on design and performanceKnowledge of:
a. Solar window
b. Local conditions (e.g., weather, soiling)
c. Peak sun hours
d. Shading analysis
e. Array orientation

### Task 5: Identify equipment specification dataKnowledge of:
a. PV module specifications (e.g., temperature coefficient, Standard Test Conditions [STC], irradiance impact on current)
b. Inverter specifications (e.g., voltage, current, surge)
c. Voltage and current ratings
d. Battery (e.g., capacity, max current)

### Task 6: Describe the function of typical components in PV systemsKnowledge of:
a. PV system configuration
b. PV modules
c. Inverters
d. Module-level electronics (e.g., microinverter,  PV optimizer)
e. Overcurrent protection devices
f. PV racking systems
g. Charge controllers
h. Batteries
i. Rapid shutdown equipment
j. Electrical materials (e.g., conduits,  conductors, disconnects)
k. Point of interconnection (e.g., supply side, load side)
l. Monitoring equipment
m. General balance of system (e.g., combiner  boxes, disconnects)
n. Grounding and bonding elements (e.g., equipment grounding conductor, grounding electrode conductor, grounding electrode, bonding jumper)

### Task 7: Explain PV system sizing considerationsKnowledge of:
a. Derating factors
b. System losses
c. Power rating
d. Energy production
e. Energy efficiency
f. Electric service infrastructure
g. Electrical codes
h. Interconnection requirements
i. Load analysis
j. Software sizing tools
k. String configuration
l. Inverter ratings

### Task 8: Read an electrical diagram of a PV systemKnowledge of:
a. Electrical symbols
b. Electrical equipment (e.g., wire, conduits, raceways, disconnects)
c. PV circuit terminology
d. String configuration
e. Inverter input window
f. Equipment nameplate ratings
g. Conductor properties (e.g., temperature ratings, ampacity ratings, UV resistance, moisture rating)

### Task 9: Recognize structural requirements of PV systemsKnowledge of:
a. Local building codes
b. Static and dynamic loads (e.g., wind)
c. Rooftop conditions (e.g., temperature, age, warranty)
d. Roofing systems (e.g., rafter, trusses, membranes)
e. Footing requirements
f. PV racking systems (e.g., ground mount,  roof mount, pole mount)
g. Mechanical fasteners
h. Building-integrated PV options



## Domain IV: Installation 29%

### Task 1: Identify the elements of a complete site‐specific safety planKnowledge of:
a. Electrical hazards and control methods (e.g., electrical shock, arc flash, de-energization plan, lockout/tagout, energized electrical work permit)
b. Use of multimeter
c. Fall hazards and protection systems  (e.g., personal fall arrest systems, guardrails, scaffolding, skylight guards)
d. Safe roof access (e.g., use of ladders, equipment handling techniques, hoisting and rigging methods)
e. Proper lifting techniques
f. Vehicle safety and equipment transport  (e.g., heavy equipment)
g. Equipment staging (e.g., roof load distribution)
h. Personal protective equipment (PPE) (e.g., hard hats, safety glasses, gloves, ear protection, footwear)
i. Environmental hazards (e.g., heat illness, lightning, wind)
j. Digging hazards (e.g., when trenching, when installing ground mount systems, location of underground utilities)
k. Safe use of hand and power tools
l. Battery safety (e.g., insulated tools, face guard, chemical goggles, eye wash, gloves, aprons)
m. Emergency response plan and accident reporting procedures
n. Required on-site documentation (e.g., injury and illness prevention program, safety data sheets)
o. Safety regulations (e.g., OSHA 29 CFR 1926, NFPA 70E)

### Task 2: Identify the elements of the plan setKnowledge of:
a. Site plan and array layout plan
b. Electrical diagrams (e.g., one-line or three-line diagrams, wiring or string diagrams)
c. Equipment data sheets and installation instructions
d. Structural details (e.g., rafter layout)
e. Permit documents
f. Site safety plan

### Task 3: Identify the elements of racking installationKnowledge of:
a. Manufacturer manuals and specifications (e.g., torque specifications)
b. Types of roofing materials (e.g., composition, tile, metal, shake, built-up)
c. Fire classification of roofing materials (e.g., Class A, B, C)
d. Roof and building construction vocabulary (e.g., rafter, truss, stud, decking, underlayment, low-slope, steep-slope, ridge, valley)
e. Types of racking systems (e.g., roof-mounted, flush-mounted, tilt-mounted, attachments, rail-less, ballasted, pole-mounted, building integrated, sun tracking)
f. Structural considerations (e.g., span, rail cantilever, module overhang, point load, dead load, live load, wind load, wind category, snow load, roof zones)
g. Fastener types and sizes (e.g., wood-lag bolts, GRK fasteners; metal-self-drilling screws, concrete; material-stainless steel, galvanized; grade and strength markings)
h. Components of different mounting systems (e.g., attachments, flashing, rails, splices or expansion joints, clamps)
i. Bonding methods (e.g., integrated grounding, WEEBs, star washers, jumpers)
j. Impact of standoff distance on energy performance
k. Waterproofing and weather sealing methods (e.g., flashing)
l. Common power and hand tools
m. Potential impact of installation on roof warranties

### Task 4: Identify the elements of electrical component installationKnowledge of:
a. Types of electrical components (e.g., inverters, hybrid inverters, microinverters, optimizers, charge controllers, disconnects, switchgear)
b. Manufacturer manuals and specifications (e.g., termination torque specifications, NEMA ratings, sunlight exposure, application of antioxidant)
c. Product intended use (e.g., indoor v
s. outdoor fittings)
d. Working clearances
e. Common electrical fittings (e.g., connectors, couplings, grounding bushings, strain reliefs, LBs)
f. Labeling and marking requirements (e.g., durability, mounting methods)
g. Grounded vs. ungrounded system requirements
h. Spacing and materials of raceway support
i. Importance of managing wires off the roof and out of the sun
j. Wiring best practices (e.g., drip loops, service loops, minimum bend radius)
k. Corrosion of dissimilar metals and value of antioxidant
l. Documentation of module-level power electronics (e.g., serial numbers of microinverters, DC optimizers)
m. Common power and hand tools

### Task 5: Identify the elements of energy storage component installationKnowledge of:
a. Types of batteries (e.g., flooded lead acid, sealed lead acid, lithium)
b. Types of charge controllers (e.g., pulse width modulation, maximum power point tracking, load-diversion)
c. Manufacturer manuals and specifications (e.g., charging settings)DOMAIN IV: INSTALLATION
d. Battery bank location and protection (e.g., venting, insulation)
e. Labeling requirements
f. Balance of system components (e.g., disconnects, cabling)

### Task 6: Identify the elements of the system commissioning procedureKnowledge of:
a. Visual inspection
b. Measurement conditions (e.g., irradiance, temperature)
c. Relevant electrical measurements (e.g., string voltage, polarity)
d. Measured performance against expectations
e. Proper operation of all electrical equipment
f. Proper operation of monitoring equipment and performance metering
g. Documentation of testing/commissioning and inspection
h. Customer orientation (e.g., walk-through, training, monitoring, expected operations and maintenance)DOMAIN IV: INSTALLATION


## Domain V: Maintenance and Operation 18%


### Task 1: Identify commonly used electrical test equipment and its purposeKnowledge of:
a. Multimeters (e.g., current, voltage, resistance, continuity)
b. Insulation testing devices (e.g., megohmmeter)
c. IV curve tracer
d. Infrared thermometer (e.g., module, breaker, connection temperature measurement)
e. Irradiance meter
f. Battery capacity testing devices (e.g., load tester)
g. Hydrometer

### Task 2: Demonstrate the ability to analyze simple electrical circuitsKnowledge of:
a. Ohm’s law
b. Power formulas (e.g., Watt’s law)

### Task 3: Describe the effects of performance parameters that are commonly monitored for PV systemsKnowledge of:
a. Temperature
b. Wind speed
c. IV curve characteristics (e.g., short circuit,  open circuit)
d. Irradiance
e. Inverter AC and DC voltage
f. Utility voltage and frequency
g. Battery voltage

### Task 4: Describe different types and elements of system performance monitoring equipmentKnowledge of:
a. Monitoring methods (e.g., web-based, on-site)
b. Current transformer
c. Voltage sense
d. Internet access (e.g., cellular gateways, network hub)
e. Revenue grade meter
f. Power line adapters
g. Battery temperature sensor
h. Weather monitoring systems (e.g., pyranometer, anemometer, thermometer)
i. Emerging technology trends
j. Supervisory control and data acquisition (SCADA) (e.g., data logging)

### Task 5: Identify common factors that result in deviations from expected system performanceKnowledge of:
a. Test conditions (e.g., Standard Test Conditions [STC], PVUSA Test Conditions [PTC],  nominal operating cell temperature [NOCT], California Energy Commission [CEC])
b. Installation practices
c. Electrical connection faults
d. Production losses and their causes (e.g., weather, pollution, shading, animals, soiling, system degradation)
e. Production enhancements and their causes (e.g., albedo, edge of cloud, low temperature, high altitude)

### Task 6: Describe typical maintenance requirements for PV systemsKnowledge of:
a. Record keeping (e.g., system logging, data sheets, user manuals, return merchandise authorization [RMA] process, maintenance plan)
b. Equipment replacement (e.g., installation practices, lockout/tagout, service life)
c. Equipment upkeep requirements (e.g., installation practices, lockout/tagout, torque requirements, module cleaning)
d. Visual inspection

### Task 7: Identify the safety requirements for operating and maintaining different types of PV systemsKnowledge of:
a. Electrical hazards and control methods (e.g., electrical shock, arc flash, de-energization plan, lockout/tagout, ground fault)
b. Use of multimeter
c. Fall hazards and protection systems (e.g., personal fall arrest systems, guardrails, scaffolding, skylight guards)
d. Safe roof access (e.g., use of ladders, equipment handling techniques, hoisting and rigging methods)
e. Proper lifting techniques
f. Equipment staging (e.g., roof load distribution)
g. Personal protective equipment (PPE) (e.g., hard hats, safety glasses, gloves, ear protection, footwear)
h. Environmental hazards (e.g., heat illness, lightning, wind, animal encounters)
i. Safe use of hand and power tools
j. Battery safety (e.g., insulated tools, face guard, chemical goggles, eye wash, gloves, aprons)
k. Emergency response plan and accident reporting procedures
l. Required on-site documentation (e.g., injury and illness prevention program, safety data sheets)
m. Electric shock risk associated with washing PV modules
n. Risk in working with energized and/or faulty equipment
o. Safety regulations (e.g., OSHA 29 CFR 1926, NFPA 70E)DOMAIN V: MAINTENANCE AND OPERATION

