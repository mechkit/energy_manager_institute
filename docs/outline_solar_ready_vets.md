# Solar Ready Vets Curriculum Design

## Introduction:   The solar energy industry is experiencing significant growth and holds promise as a sound career direction for individuals across multiple types of fields and professional backgrounds.  The U.S. Armed forces offers a steady source of talented individuals with vast array of leadership experience and disciplines that offer an excellent source of talent that could serve the solar industry.

The Purpose of the Solar Ready Veteran (SRV) Program is:  To : prepare veterans across all divisions of the armed services for transition into the solar industry in a way that jointly serves veterans and the solar industry so that mutually benefits veterans seeking meaningful employment and solar industry members seeking to expand and improve their professional workforce.
Program Design:  The 5-week SRV program includes four sets of program learning goals aligned around (1) the NABCEP Entry Level body of knowledge; (2) gaining hands-on experience with solar system site analysis, design, installation, commissioning, operation, maintenance and financial considerations; (3) Safety issues unique to solar + OSHA 30; (4) Transition planning and individual support of entry into the solar industry. These goals, and the learning objectives associate with each, are pursued in parallel during the course.

## Course Objectives:  Upon completion of this course, participants will be:
-
- Knowledgeable of fundamental photovoltaic systems concepts and learning objectives defined in the North American Board of Certified Energy Practitioners (NABCEP).
- Experienced in core processes of solar PV projects including basic site analysis, system design, system assembly, commissioning, inspection, maintenance, and financial analysis,
- Familiar with the solar industry organization, career paths, and job opportunities that leverage their unique skills, traits, and interests.
- Certified as completing OSHA 30 Safety training

## Course Materials:  Course materials include handouts to support note taking during class presentations and activities, online materials and resources, and the course textbook:  “Photovoltaic Systems”, 3rd Edition by James Dunlap.
Hands-on Learning Experiences and Labs:  The SRV program provides hands on experience using actual PV systems.  Four sets of equipment are required for select activities including site analysis, electrical measurement devices and systems for assembly (grid tied/grounded, micro inverter, battery systems.  At least one permanent system with monitoring data should also be accessible to students, and one nearby utility scale system (preferably on base) is suggested for case study and site visit purposes.

## Content Overview

### Block 1:  Technical PV Fundamentals (NABCEP Entry Level Exam + Expanded SRV LO’s):  Provide participants with working understanding of PV technology, systems configuration, and solar resource assessment.  This course block provides industry recognized credential indicating basic vocabulary and knowledge of PV fundamentals, and participation in a formal learning process.

PV Block 1:  PV Market and Applications:  Objectives include defining PV system types (PV direct,  stand-alone and grid-tied  and battery back-up  systems), key features and benefits of different PV system, variability of Residential,  commercial and utility scale PV systems, centralized vs decentralized power systems, relationship between energy conservation and efficiency
PV Block 2:  Safety:  Objectives include recognition of hazards unique to solar projects and abilty to conduct site-specific hazard analysis.
PV Block 3: Electricity Basics: Objectives include basic knowledge of electrical theory as it applies to PV systems including Current, voltage and resistance, OHMS Law, function and purpose of common electrical devices and equipment, basic electrical testing tools, Utility systems structure
PV Block 4: Solar Energy Fundamentals: Objectives include basic knowledge necessary to  determine the best means of  maximizing the amount of  solar power available at a given site
PV Block 5:  PV Module Fundamentals:  Objectives include ability to describe the design, manufacturing and operation of PV modules, module types, construction, IV curves, and calculation of temperature impacts, efficiency, power density, and understanding of performance ratings, standards.
PV Block 6:  PV System Components: Objectives include describe the design and operation of the major components including PV modules, inverters, charger controllers, chargers, load centers, batteries.
PV Block 7:  PV System Sizing Principles:  Objective is to  teach students how to size PV systems (grid-tied and standalone)
PV Block 8:  PV System Electrical Design:  Objectives is to enable students to do simple designs of PV systems according to the most basic requirements of the NEC
PV Block 9  PV System Mechanical Design:  Objective is to provide students with an overview of PV mounting systems including roof types and construction and mounting systems
PV Block 10:  Performance Analysis, Maintenance, and Troubleshooting:  Objective is to understand processes required for evaluating performance and the management of inspection, commissioning, operation, and maintenance.
PV Block XX:  Operations and Maintenance: Understand the economic drivers for the implementation of an O&M Program, identify modes of failure, discuss the set-up, documentation, calculations, and tools used in an O&M program
PV Block 11: Marketing, Sales, and Business Development: Objective include familiarization with solar project development processes and markets, sales methods, and structure of solar industry organizations.
PV Block 12:  Financial Analysis Methods and Tools:  Objective is to be familiar with the role of financial analysis in solar project development and to be experiences with the use of basic solar financial analysis tools.
PV Block 13:  Basic math refreshers:  Objective is ability to perform fundamental mathematical functions needed in PV system design and evaluation.
PV Block 14:  NABCEP Exam Review and Completion:  Objective is to prepare for online participation of NABCEP Entry-level examination

### Block 2:  Hands-on PV Systems Design, Construction, Operations, and Maintenance Activities:  Provide hands-on experience with PV system components, site assessment, and system diagnostics.  This course block reinforces systems understanding and introduces processes used by solar professionals for installation, operation, and maintenance.

- Hands on Activity 1:  Site survey and analysis: Objectives include Identify factors to consider when choosing array locations, including shading, accessibility, structural concerns, and existing electrical equipment.
- Hands-on Activity 2:  Module characterization:  Includes Measure of PV module output and IV curve and experience using electrical testing equipment to evaluate PV module performance
- Hands-on Activity 3:  System components:  Familiar with major PV system components, name plate characteristic, including PV modules and arrays, inverters and chargers, charge controllers, energy storage and other sources.
- Hands-on Activity 4:  PV System exploration:  Examine an existing system or systems and the specific technical features of the inverter and modules.  Identify the maximum and minimum string size for the paired module and inverter.
- Hands on Activity 5 :  System Assembly and Building Integration:  Includes assembly of multiple types of PV systems, inverters, and racking from system orientation, assembly, grid interconnection.
- Hands-on Activity 6:  Commissioning:  Includes the procedures to systematically evaluate the performance of a PV system to ensure proper operation.
- Hands-on Activity 7:  System Design:  String Inverter Matching:  Includes determine the maximum and minimum number of modules that may be used in source circuits and the total number of source circuits that may be used with a specified inverter
- Hands-on Activity 8:  PV system operations and maintenance: includes an overview of O&M services, typical activities and measurements, the design of an O&M plan, and practice performing O&M activities on an existing system operating in proximity to the class location
- Hands-on Activity 10:  PV system financial analysis: includes experience using financial analysis tools to evaluate feasibility of solar and support sales and how incentives can encourage investment in PV
- Hands-on Activity 11:  Case Study and Field Trip: includes an examination of a utility scale installation nearby, including development process, sitework, construction, performance, and operations/ maintenance.  Project details and documentation are reviewed and coordinated with a site visit.

### Block 3:  PV System Safety and OSHA 30:  Provide a comprehensive overview of construction safety with an emphasis on electrical construction.  This course block provides industry recognized credential and perquisite to advanced safety credentials needed on solar construction projects.

- PV Safety OSHA 30 Part 1: Introduction to OSHA, Contractor's Safety and Health Program Reporting and Record Keeping
- PV Safety OSHA 30 Part 2 : Electrical Hazards, Fall Protection, Materials Handling
- PV Safety OSHA 30 Part 3: Cranes and Rigging, Motorized Mobile Platforms, The Competent Person
- PV Safety OSHA 30 Part 4:  Excavations, Work Zone Traffic Control, Forklift Hazards

### Block 4:  Transition Solar:  Provide an understanding of the solar industry organization, market growth, and professional development pathways Support the identification of entry points and career trajectories that are aligned with the unique skills and interests.

- Transition Solar Block 1 -  Skype Sessions with Veteran in Industry:   Objectives include: Describe the perspective of veterans and their transition out of the military into the solar industry
- Transition Solar Block 2 - Solar resume design and strategy: Objectives include Represent individual backgrounds, skills, and education in a solar industry-focused resume and professional development plan. Author career objectives customized to specific job and career opportunities
- Transition Solar Block 3 - Solar Career Mapping:
Objectives include: Gain experience using resources that are designed to help identify entry points and pathways in solar design, construction, manufacturing, and project development.
Gain experience in the use of career and job placement tools and services targeting veterans.
Gain experience in the self-assessment of core values and contributions.
Gain experience with solar Foundation workforce research
- Transition Solar Block 4 - Solar industry organizations and professional associations:  Objectives include Gain experience in the evaluation of solar companies, industry organizations, NGO’s and respective markets, strategies, and growth trajectories, and Human Resource needs
- Transition Solar Block 5 - Using the Post 9-11 GI Bill to enter the Solar Industry:  Objectives include Understand and apply for the post 9-11 GI Bill and the pathways it supports.
- Transition Solar Block 6 - Interview Skill and Job Search:  Objectives include Confidently participate in both informal and formal interview settings. Develop ability to communicate personal strengths and aspirations, understand methods to follow up with opportunities.

## Course Delivery and Organization

Each week of the SRV program is designed to support the concurrent achievement of the four course objectives and learning blocks.   It is expected that the day to day content of this 5 week program may vary slightly based on instructor preferences.   A summary of the major milestones and objectives for each week of the SRV program is described below:

### WEEK 1
- Block 1 - Technical Fundamentals:  The goal of this week is to introduce students to the foundation concepts in which PV systems are deployed, and the electrical characteristics and cost trends of PV modules and installed $/kwH.  In week 1, participants gain a fundamental understanding of solar energy and the solar industry.
- Block 2 - Hands-on:  Activities in this week compliment classroom learning and include visiting a rooftop PV system, preview of lab equipment that will be used in the course, and experimentation with basic electrical measuring devices and module performance.
- Block 3 - Safety:  Students are introduced to electrical hazards associated with PV systems and sites complete 7.5 hours of OSHA 30 training
- Block 4 - Transition:  Students are familiarized with the solar industry and resources that support job placement.  The design of a solar-specific resume is initiated.

### WEEK 2
- Block 1 - Technical Fundamentals:  The goal of week 2 is to familiarize students with the components of PV systems including inverters, modules, and balance of systems.  The translation of modules into arrays, and the calculations of associated electrical output are also included.  Week 2 builds directly off of the understanding of PV module output from week 1
- Block 2 - Hands-on:  Week 2 activities are centered upon providing experiences with all types of PV system components through the  inspection of components and preparatory activities leading to the assembly of multiple types of PV systems including grounded and ungrounded inverters, micro inverters, and off-grid systems.
- Block 3 - Safety:  Students are introduced to electrical hazards associated with PV systems components and complete 7.5 hours of OSHA 30 training
- Block 4 - Transition:  Students interact with veterans who work in the solar industry and explore their unique strengths and passions that need to be communicated through resumes and interviews.

### WEEK 3
- Block 1 - Technical Fundamentals:  The goal of week 3 is to provide participants with design concepts and codes / regulations that apply to the PV industry, as well the financial considerations, sales, and marketing  that apply to the design of PV systems.  Week 3 builds off of the familiarity of system components provided in week 2.
- Block 2 - Hands-on:  Week 3 activities are centered upon gaining experience with design procedures including string inverter matching, conductor sizing while working with actual systems.  Commissioning procedures and activities are also practiced. Instructional systems built be students and existing systems at location are used as context.
- Block 3 - Safety:  Students are introduced to electrical hazards associated with PV system construction and complete 7.5 hours of OSHA 30 training
- Block 4 - Transition:  Students are familiarized with Post 911 GI Bill and complete the development of resume and career planning.

### WEEK 4
- Block 1 - Technical Fundamentals:  The goal of week 4 is to provide students with experience evaluating the performance of PV systems, and to gain an understanding of operations, and maintenance services of PV systems.  Additionally, Marketing sales, and O&M services are presented.   Week 4 builds upon both the knowledge of PV system components in week 2, and the design experiences in week 3.
- Block 2 - Hands-on:  Week 4 activities focus on the diagnostic procedures needed to evaluate the installation and operation of PV systems against performance goals as well as compliance with codes.  Typical operations and maintenance procedures are practiced on instructional equipment.
- Block 3 - Safety:  Students are introduced to electrical hazards associated with PV systems commissioning and inspection, and complete 7.5 hours of OSHA 30 training
- Block 4 - Transition:  Students evaluate various types of companies in the solar industry including background research in preparation for interviews.

### WEEK 5
- Block 1 - Technical Fundamentals:  The goal of week 5 is to facilitate the synthesis of the course topics into a systems-understanding of solar energy technology and markets reinforced with a case study and site visit to a significant PV installation as well as review and administration of the NABCEP ELE exam.
- Block 2 - Hands-on:  Week 5 activities include the evaluation of a large scale PV project design, construction, and performance, and a site visit to the system.
- Block 3 - Safety:  Students review fundamental PV systems safety per block 2 of the NABCEP Entry Level body of Knowledge
- Block 4 - Transition:  Students gain experience meeting with and discussing career and job opportunities in the solar industry.

## Example Curriculum Outline
Day
AM Topic
PM Topic
Resources
1
Kick-off / Entry processing
Block 1 – PV Markets and Applications
Text, online materials
2
Block 3 – Electricity Basics / Tour of solar array
Block 4 – Solar Energy Fundamentals Part 1
Text, online materials
3
Block 4 – Solar Energy Fundamentals Part 2
Activity: Site survey and analysis
Text, online materials
4
Block 5 -  PV Module Fundamentals
Activity:  PV module characterization
Text, online materials
5
OSHA 30
OSHA 30
Text, online materials
6
Block 7 – PV System Components
Block 7 – PV System Components
Text, online materials
7
Block 7 – PV System Components
Activity:  PV System components exploration
Text, online materials
8
Mid Term Review
Mid Term Exam
Text, online materials
9
Solar Career Resources research
Solar resume development
Text, online materials
10
OSHA 30
OSHA 30
Text, online materials
11
Block 9 – PV System Mechanical Design
Activity: PV System Assembly
Text, online materials
12
Activity: PV System Assembly (Cont. )
Activity: Commissioning, Trouble Shooting
Text, online materials
13
Block 8 – PV Electrical Design
Activity: String inverter matching
Text, online materials
14
Block 10 – Permitting and Inspection
Activity : Inspection procedures (rooftop systems)
Text, online materials
15
OSHA 30
OSHA 30
Text, online materials
16
Block 10: Trouble Shooting, Operations, and Maintenance
Activity: Operations, and Maintenance (rooftop systems)
Text, online materials
17
Activity: Field trip to PV site


18
Block 11 : PV System customer, sales, marketing, and development
Block 11: PV System customer, sales, marketing, and development (cont. )
Text, online materials
19
Block 12:  PV System Financial Analysis
Activity : Financial Analysis tools
Text, online materials
20
OSHA 30
OSHA 30
Text, online materials
21
Practice exam
Exam review

22
Exam review
NABCEP ELE Exam (online format)

23
Company Research
Interview Skills
Text, online materials
24
Meetings with Solar Companies
Interviews

25
Interviews / final course evaluation discussion
Completion / Graduation

Curriculum Content and LMS
SRV instructors are supported through an online content and learning management system.  The purposed of this system is to:
- Organize presentation and activity materials in a manner that is aligned with the curriculum design
- Support daily learning check quizzes and mid-term examinations
- Enable blended learning delivery
- Support updating of materials serving a community of instructors.
The base LMS system is designed in Moodle, and available for export to in-house LMS systems used by colleges and universities as SCORM compliant modules.
Four set of resources are included that can be adopted folly or on an a-la-carte basis depending on the level of existing materials and expertise at a given delivery location.  These resources include:
    1) Curriculum Design including description, learning objectives, resources, and assessment methods used for the four sets of learning blocks.
    2) PowerPoint presentations adopted from Jim Dunlap’s instructor resource guide and organized into individual presentations aligned with Block 1 Technical content.
    3) Resources including articles, websites, example documents, and reference manuals.
    4) Online quizzes aligned with each Block 1 topic and formatted to emulate NABCEP Entry-level exam topics and questions.  Questions for quizzes, mid-term, and practice exam are drawn from a central database of 120 questions.
    5) Online content and learning modules: Alternative content and activities that can be utilized as alternative to lecture content.  Includes online materials, videos, and additional resources.


Contents
Technical Block 1: PV Markets and Applications	11
Technical Block 3: Electricity Basics	12
Technical Block 4: Solar Energy Fundamentals	13
Technical Block 5: PV Module Fundamentals	15
Technical Block 6: System Components	17
Technical Block 7: PV System Sizing Principles	18
Technical Block 8: PV System Electrical Design	19
Technical Block 9: PV System Mechanical Design	21
Technical Block 10: Performance Analysis, Maintenance, and Troubleshooting	23
Technical Block 11: Marketing, Sales, and Business Development	25
Technical Block 12: Financial Analysis and Tools	26
Technical Block 13: Math Refresher	27
Technical Block 14: NABCEP Exam Review and Completion	28
PV Entry Level Practice Exams	28
NABCEP PV Installation Professional Resource Guide (practice questions relevant	28
to Entry Level learning objectives only)	28
Activity 1: Site Survey and Analysis	29
Activity 2: Module Characterization	30
Activity 3: PV System Components	31
Activity 4: PV System Exploration	32
Activity 5: System Assembly and Building Integration	33
Activity 6: System Commissioning	34
Activity 7: System Design:  String and Inverter Matching	35
Activity 8: System Inspection	36
Activity 9: PV Systems Operations and Maintenance	37
Activity 10: PV System Financial Analysis	38
Activity 11: Case Study and Site visit:  Armed Forces PV Installation	39
Transition Solar Block 1: Veterans in the Solar Industry	40
Transition Solar Block 2: Solar Resume Design and Strategy	41
Transition Solar Block 3: Solar Career Mapping	42
Transition Solar Block 4: Solar industry organizations and professional associations	43
Transition Solar Block 5: Post 9/11 GI Bill	44
Transition Solar Block 6: Interview Skills and Job Search	45


Technical Block 1: PV Markets and Applications

Description Provide the students with an overview of the PV market and applications.  Key concepts are:
- PV system definition (PV direct,  stand-alone and grid-tied  and battery back-up  systems)
- Key features and benefits of different PV systems
- Residential, commercial and utility scale PV systems
- Centralized vs decentralized power systems
- Energy conservation and efficiency
Objectives: (Based on NABCEP PV Entry Level)
    1 Identify key contributions to the development of PV technology.
    2 Identify common types of PV system applications for both stand-alone and utility interactive systems with and without energy storage.
    3 Associate key features and benefits of specific types of PV systems, including residential, commercial, BIPV, concentrating PV, and utility-scale.
    4 List the advantages and disadvantages of PV systems compared to alternative electricity generation sources.
    5 Describe the features and benefits of PV systems that operate independently of the electric utility grid.
    6 Describe the features and benefits of PV systems that are interconnected to and operate in parallel with the electric utility grid.
    7 Describe the roles of various segments of the PV industry and how they interact with one other.
    8 Understand market indicators, value propositions, and opportunities for both grid-tied and stand-alone PV system applications.
    9 Discuss the importance of conservation and energy efficiency as they relate to PV system applications.
Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 1 – Introduction to Photovoltaic Systems
- Solar Energy International, “Solar Electric Handbook.” Unit 1 – Overview of Renewable Energy and the Solar Industry
- Penn State online course “Photovoltaic System Design and Construction.” Unit 1.1 – Introduction and Application of PV Systems
- SunShot Identity Video:  http://energy.gov/eere/sunshot/about
- Energy 101: Solar PV: http://energy.gov/eere/videos/energy-101-solar-pv
- Solar Professional magazine: http://solarprofessional.com/
- “Three Perspectives on the Solar Industry” https://www.youtube.com/watch?v=q2_OKGm_TOE
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.

Technical Block 3: Electricity Basics

Description: Provide students with a basic knowledge of electrical theory as it applies to PV systems. Key concepts include:
- Current, voltage and resistance
- Ohm’s Law
- The function and purpose of common electrical devices and equipment
- The ability to understand and use basic electrical testing tools
- Utility systems structure (generation, transmission,  distribution and delivery)
Objectives: (Based on NABCEP PV Entry Level)
    1 Understand the meaning of basic electrical parameters including electrical charge, current, voltage, power and resistance, and relate these parameters to their hydraulic analogies (volume, flow, pressure, hydraulic power and friction).
    2 Explain the difference between electrical power (rate of work performed) and energy (total work performed).
    3 Describe the function and purpose of common electrical system components, including conductors, conduits/raceways and enclosures, overcurrent devices, diodes and rectifiers, switchgear, transformers, terminals and connectors, grounding equipment, resistors, inductors, capacitors, etc.
    4 Identify basic electrical test equipment and its purpose, including voltmeters, ammeters, ohmmeters and watt-hour meters.
    5 Demonstrate the ability to apply Ohm’s Law in analyzing simple electrical circuits, and to calculate voltage, current, resistance or power given any other two parameters.
    6 Understand the fundamentals of electric utility system operations, including generation, transmission, distribution, and typical electrical service supplies to buildings and facilities.
Resources:
- Solar Energy International, “Solar Electric Handbook.” Chapter 3 – Basics of Electricity
- Penn State online course “Photovoltaic System Design and Construction.” Unit 1.3, Module 1 – Review of Electrical Concepts
- The Khan Academy – Electricity and Magnetism: https://www.khanacademy.org/science/physics/electricity-magnetism

Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.

Technical Block 4: Solar Energy Fundamentals

Description: Provide students with the basic knowledge necessary to determine the best means of  maximizing the amount of  solar power available at a given site. Key concepts include:
- Basic solar energy terminology
- Solar radiation (spectrum and direct, diffuse and albedo)
- Measurement of available solar power
- Earth’s movement around the sun
- Longitude, latitude, magnetic declination
- Solar vs local time
- Peak sun hours
- Impact of tilt angle,  orientation and shading  on solar energy production
- Site assessment practices and tools
Objectives: (Based on NABCEP PV Entry Level)
    1 Define basic terminology, including solar radiation, solar irradiance, solar irradiation, solar insolation, solar constant, air mass, ecliptic plane, equatorial plane, pyranometer, solar declination, solstice, equinox, solar time, solar altitude angle, solar azimuth angle, solar window, array tilt angle, array azimuth angle, and solar incidence angle.
    2 Diagram the sun’s apparent movement across the sky over any given day and over an entire year at any given latitude, and define the solar window.
    3 For given dates, times and locations, identify the sun’s position using sun path diagrams, and determine when direct solar radiation strikes the north, east, south and west walls and horizontal surfaces of a building.
    4 Differentiate between solar irradiance (power), solar irradiation (energy), and understand the meaning of the terms peak sun, peak sun hours, and insolation.
    5 Identify factors that reduce or enhance the amount of solar energy collected by a PV array.
    6 Demonstrate the use of a standard compass and determine true geographic south from magnetic south at any location given a magnetic declination map.
    7 Quantify the effects of changing orientation (azimuth and tilt angle) on the amount of solar energy received on an array surface at any given location using solar energy databases and computer software tools.
    8 Understand the consequences of array shading and best practices for minimizing shading and preserving array output.
    9 Demonstrate the use of equipment and software tools to evaluate solar window obstructions and shading at given locations, and quantify the reduction in solar energy received.
    10 Identify rules of thumb and spacing distances required to avoid inter-row shading from adjacent sawtooth rack mounted arrays at specified locations between 9 am and 3 pm solar time throughout the year.
    11 Define the concepts of global, direct, diffuse and albedo solar radiation, and the effects on flat-plate and concentrating solar collectors.
    12 Identity the instruments and procedures for measuring solar power and solar energy.

Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 2 – Solar Radiation; Chapter 3 – Site Surveys and Preplanning.
- Solar Energy International, “Solar Electric Handbook.” Chapter 8 – Solar Site Analysis
- Penn State online course “Photovoltaic System Design and Construction.” Unit 1.2, Solar Radiation; Unit 2.1 – Array Positioning and Site Evaluation
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.

Technical Block 5: PV Module Fundamentals

Description: Describe the design, manufacturing and operation of PV modules. Key concepts include:
- PV module types (mono/poly crystalline, amorphous, CIGS, CdTe, etc)
- PV module construction
- IV curves
- Impact of irradiance and temperature on module performance
- Module efficiency / Power density
- Similar modules in series and parallel
- Dissimilar modules in series and parallel
- Performance ratings (STC, SOC, NOCT and PTC)
- PV module standards and testing
- Bypass diodes
Objectives: (Based on NABCEP PV Entry Level)
    1 Explain how a solar cell converts sunlight into electrical power.
    2 Distinguish between PV cells, modules, panels and arrays.
    3 Identify the five key electrical output parameters for PV modules using manufacturers’ literature (Voc, Isc, Vmp, Imp and Pmp), and label these points on a current-voltage (I-V) curve.
    4 Understand the effects of varying incident solar irradiance and cell temperature on PV module electrical output, illustrate the results on an I-V curve, and indicate changes in current, voltage and power.
    5 Determine the operating point on a given I-V curve given the electrical load.
    6 Explain why PV modules make excellent battery chargers based on their I-V characteristics.
    7 Understand the effects of connecting similar and dissimilar PV modules in series and in parallel on electrical output, and diagram the resulting I-V curves.
    8 Define various performance rating and measurement conditions for PV modules and arrays, including STC, SOC, NOCT, and PTC.
    9 Compare the fabrication of solar cells from various manufacturing processes.
    10 Describe the components and the construction for a typical flat-plate PV module made from crystalline silicon solar cells, and compare to thin-film modules.
    11 Given the surface area, incident solar irradiance and electrical power output for a PV cell, module or array, calculate the efficiency and determine the power output per unit area.
    12 Discuss the significance and consequences of PV modules being limited current sources.
    13 Explain the purpose and operation of bypass diodes.
    14 Identify the standards and design qualification testing that help ensure the safety and reliability of PV modules.

Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 4 – System Components and Configurations; Chapter 5 – Cells, Modules, and Arrays.
- Solar Energy International, “Solar Electric Handbook.” Chapter 4 – Components; Unit 3 – Modules, Series and Parallel, and Meters
- Penn State online course “Photovoltaic System Design and Construction.” Unit 1.3, PV Cells, Modules, and Arrays
- “Exploring a Solar Module”: https://www.youtube.com/watch?v=xQTmUv6_uMg
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.

Technical Block 6: System Components

Description: Describe the design and operation of the major components in both grid-tied and standalone PV systems.  Also, to examine how these components can be combined in different combinations to create different types of PV systems. Key concepts include:
- Major components – PV modules,  inverters, charger controllers, chargers, load centers,  batteries
- BOS equipment – Combiner/junction boxes, disconnects,  raceways/conductors and overcurrent protection devices
- Inverters
- Battery types and charging cycles
- Battery capacity factors (ampacity, days of autonomy,  temp. compensation, DOD/SOC)
Objectives: (Based on NABCEP PV Entry Level)
    1 Describe the purpose and principles of operation for major PV system components, including PV modules and arrays, inverters and chargers, charge controllers, energy storage and other sources.
    2 List the types of PV system balance of system components, and describe their functions and specifications, including conductors, conduit and raceway systems, overcurrent protection, switchgear, junction and combiner boxes, terminations and connectors.
    3 Identify the primary types, functions, features, specifications, settings and performance indicators associated with PV system power processing equipment, including inverters, chargers, charge controllers, and maximum power point trackers.
    4 Understand the basic types of PV systems, their major subsystems and components, and the electrical and mechanical BOS components required.
Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 4 – System Components and Configurations; Chapter 6 – Batteries; Chapter 7 – Charge Controllers; Chapter 8 – Inverters.
- Solar Energy International, “Solar Electric Handbook.” Chapter 4 – Components
- Penn State online course “Photovoltaic System Design and Construction.” Unit 3.1, Introduction to the NEC and PV System Components
- “Exploring Low and High Frequency Inverters”: https://www.youtube.com/watch?v=iMHBw93W-XM
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.


Technical Block 7: PV System Sizing Principles

Description: Students learn how to size PV systems (grid-tied and standalone). Key concepts include:
- String and inverter matching for grid tied PV systems
- Voltage temperature correction calculations
- Electrical loads (Startup and running power, energy consumption, duty cycles)
- Standalone PV systems (Manual calculations, critical design months, matching PV array size to loads,  determining battery requirements)
Objectives: (Based on NABCEP PV Entry Level)
    1 Understand the basic principles, rationale and strategies for sizing stand-alone PV systems versus utility-interactive PV systems.
    2 Given the power usage and time of use for various electrical loads, determine the peak power demand and energy consumption over a given period of time.
    3 Beginning with PV module DC nameplate output, list the de-rating factors and other system losses, and their typical values, and calculate the resulting effect on AC power and energy production, using simplified calculations, and online software tools including PVWATTS.
    4 For a specified PV module and inverter in a simple utility-interactive system, determine the maximum and minimum number of modules that may be used in source circuits and the total number of source circuits that may be used with a specified inverter, depending upon the expected range of operating temperatures, the inverter voltage windows for array maximum power point tracking and operation, using both simple calculations and inverter manufacturers’ online string sizing software tools.
    5 Given a stand-alone application with a defined electrical load and available solar energy resource, along with PV module specifications, size and configure the PV array, battery subsystem, and other equipment as required, to meet the electrical load during the critical design period.
Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 9 – System Sizing.
- Solar Energy International, “Solar Electric Handbook.” Unit 5 – Grid-Direct System Sizing; Unit 8 – Battery Based Systems
- Penn State online course “Photovoltaic System Design and Construction.” Unit 4.1- Grid-Tied Systems without Batteries; Unit 4.2 – Off-Grid Systems; Unit 4.3 – Emergency PV systems and Grid-Connected PV Systems with Batteries.
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.

Technical Block 8: PV System Electrical Design

Description: To enable students to do simple designs of PV systems according to the most basic requirements of the NEC.  Key concepts include:
- Basic knowledge of Article 690, Solar Photovoltaic Systems in the National Electrical Code
- PV equipment selection and specifications (PV modules,  inverters,  charge controllers,  combiner/junction boxes, disconnects and overcurrent protection)
- Preparation of simple one-line electrical diagrams
- Putting modules in series (Max voltage and inverter DC voltage limits)
- Putting modules series in parallel (building PV array size)
- Review of voltage temperature correction calculations
- NEC ampacity calculations (max current,  conductor derating factors)
- Conductor types and applications
- Voltage drop calculations
- Project documentation,  permitting, etc
Objectives: (Based on NABCEP PV Entry Level)
    1 Draw and prepare simple one-line electrical diagrams for interactive and standalone PV systems showing all major components and subsystems, and indicate the locations of the PV source and output circuits, inverter input and output circuits, charge controller and battery circuits, as applicable, and mark the directions of power flows through the system under various load conditions.
    2 Understand how PV modules are configured in series and parallel to build voltage, current and power output for interfacing with inverters, charge controllers, batteries and other equipment.
    3 Identify basic properties of electrical conductors including materials, size, voltage ratings and insulation coverings and understand how conditions of use, such as location, other conductors in the same conduit/raceway, terminations, temperature and other factors affect their ampacity, resistance and corresponding overcurrent protection requirements.
    4 Understand the importance of nameplate specifications on PV modules, inverters and other equipment on determining allowable system voltage limits, and for the selection and sizing of conductors, overcurrent protection devices, disconnect means, wiring methods and in establishing appropriate and safe interfaces with other equipment and electrical systems.
    5 Determine the requirements for charge control in battery-based PV systems, based on system voltages, current and charge rates.
    6 Identify the labeling requirements for electrical equipment in PV systems, including on PV modules, inverters, disconnects, at points of interconnection to other electrical systems, on battery banks, etc.
    7 Understand the basic principles of PV system grounding, the differences between grounded conductors, grounding conductors, grounding electrode conductors, the purposes of equipment grounding, PV array ground-fault protection, and the importance of single-point grounding.
    8 Apply Ohm’s Law and conductor properties to calculate voltage drop for simple PV source circuits.
    9 Identify the requirements for plan review, permitting, inspections, construction contracts and other matters associated with approvals and code-compliance for PV systems.
    10 Demonstrate knowledge of key articles of the National Electrical Code, including Article 690, Solar Photovoltaic Systems.

Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 9 – System Sizing; Chapter 10 – Electrical Integration; Chapter 12 – Utility Interconnection.
- Solar Energy International, “Solar Electric Handbook.” Unit 6 – Grounding, Wiring, and Overcurrent Protection.
- Penn State online course “Photovoltaic System Design and Construction.” Unit 3.2 – Electrical Integration and Code Compliance; 4.1- Grid-Tied Systems without Batteries; Unit 4.2 – Off-Grid Systems; Unit 4.3 – Emergency PV systems and Grid-Connected PV Systems with Batteries.
- “Solar PV Inspection Walkthrough” video series: https://www.youtube.com/watch?v=APrbl0Ngp8o&index=1&list=PLa8_mvXW28yLqZjgEidnbEjBFgD1mFIl9
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.


Technical Block 9: PV System Mechanical Design

Description: Provide students with an overview of PV mounting systems.  Key concepts include:
- Intro to roof types and construction
- Description of different mounting systems (fixed flush, ballasted,  attached, ground mount,  trackers and BIPV)
- Pros and cons of different mounting systems
- Intro to design loads (live, dead, wind, snow, seismic)
- PV array layout considerations
- Geotechnical issues
- Fire safety requirements
- Rack manufacturer installation instructions
Objectives: (Based on NABCEP PV Entry Level)
    1 Identify the common ways PV arrays are mechanically secured and installed on the ground, to building rooftops or other structures, including rack mounts, ballasted systems, pole mounts, integral, direct and stand-off roof mounts, sun tracking mounts and for other building-integrated applications.
    2 Compare and contrast the features and benefits of different PV array mounting systems and practices, including their design and materials, standardization and appearance, applications and installation requirements, thermal and energy performance, safety and reliability, accessibility and maintenance, costs and other factors.
    3 Understand the effects on PV cell operating temperature of environmental conditions, including incident solar radiation levels, ambient temperature, wind speed and direction for various PV array mounting methods.
    4 List various building-integrated PV (BIPV) applications and compare and contrast their features and benefits with conventional PV array designs.
    5 Identify desirable material properties for weathersealing materials, hardware and fasteners, electrical enclosures, wiring systems and other equipment, such as UV, sunlight and corrosion resistance, wet/outdoor approvals and other service ratings appropriate for the intended application, environment and conditions of use, and having longevity consistent with the operating life expectancies of PV systems.
    6 Understand the requirements for roofing systems expertise, and identify the preferred structural attachments and weathersealing methods for PV arrays affixed to different types of roof compositions and coverings.
    7 Identify the types and magnitudes of mechanical loads experienced by PV modules, arrays and their support structures, including dead loads, live loads, wind loads, snow loads, seismic loads, in established combinations according to ASCE 7-05 Minimum Design Loads for Buildings and Other Structures.
    8 Identify PV system mechanical design attributes that affect the installation and maintenance of PV arrays, including hardware standardization, safety and accessibility, and other factors.
    9 Identify mechanical design features that affect the electrical and thermal performance of PV arrays, including array orientation, mounting methods and other factors.
    10 Review and recognize the importance of PV equipment manufacturers’ instructions with regard to mounting and installation procedures, the skills and competencies required of installers, and the implications on product safety, performance, code-compliance and warranties.

Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 10 – Mechanical Integration.
- Solar Energy International, “Solar Electric Handbook.” Chapter 9 – Mounting; Chapter 10 – Roofing Systems.
- Penn State online course “Photovoltaic System Design and Construction.” Unit 2.2 – System Mounting and Building Integration.
- National Roofing Contractors Association Guidelines for Roof Systems with Rooftop Photovoltaics System Components
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.

Technical Block 10: Performance Analysis, Maintenance, and Troubleshooting

Description: Provide a basic understanding of how to analyze the PV system performance. Also, the learning objective provides the students an introduction to PV operation and maintenance, and trouble-shooting activities.  Key concepts are:
- Intro to roof types and construction
- PV system performance indicators
- Energy production modeling vs actual performance
- Common PV system failure modes
- Safety equipment for PV system analysis work
- Typical O&M requirements and scheduling
- Use of equipment manuals for troubleshooting work.
-  Discuss various potential problems related to PV system design, components, installation, operation or maintenance that may affect the performance and reliability of PV systems.
- Understand the safety requirements for operating and maintaining different types of PV systems and related equipment.
- Identify and describe the use and meaning of typical performance parameters monitored in PV systems, including DC and AC voltages, currents and power levels, solar energy collected, the electrical energy produced or consumed, operating temperatures and other data.
- Compare PV system output with expectations based on system sizing, component specifications and actual operating conditions, and understand why actual output may be different than expected.
- Understand basic troubleshooting principles and progression, including recognizing a problem, observing the symptoms, diagnosing the cause and taking corrective actions leading from the system, subsystem to the component level.
- Identify the most common types of reliability failures in PV systems and their causes due to the equipment, quality of installation and other factors.
Objectives: (Based on NABCEP PV Entry Level)
    1 Discuss various potential problems related to PV system design, components, installation, operation or maintenance that may affect the performance and reliability of PV systems.
    2 Identify and describe the use and meaning of typical performance parameters monitored in PV systems, including DC and AC voltages, currents and power levels, solar energy collected, the electrical energy produced or consumed, operating temperatures and other data.
    3 Compare PV system output with expectations based on system sizing, component specifications and actual operating conditions, and understand why actual output may be different than expected.
    4 Describe typical maintenance requirements for PV arrays and other system components, including inverters and batteries, etc.
    5 Understand the safety requirements for operating and maintaining different types of PV systems and related equipment.
    6 Identify the most common types of reliability failures in PV systems and their causes due to the equipment, quality of installation and other factors.
    7 Review component manufacturers’ instructions for operation, maintenance and troubleshooting for PV modules and power processing equipment, and develop a simple maintenance plan for a given PV system detailing major tasks and suggested intervals.
    8 Understand basic troubleshooting principles and progression, including recognizing a problem, observing the symptoms, diagnosing the cause and taking corrective actions leading from the system, subsystem to the component level.

Resources:
- Dunlop, Jim. “Photovoltaic Systems.” Chapter 13 – Permitting and Inspection; Chatper 14 – Commissioning, Maintenance, and Troubleshooting.
- Solar Energy International, “Solar Electric Handbook.” Unit 7 – Safely Installaing and Commissioning Grid-Direct Systems.
- Penn State online course “Photovoltaic System Design and Construction.” Unit 5.2 – Commissioning, Maintenance, and Troubleshooting.
- “Solar Operation and Maintenance” video series: https://www.youtube.com/watch?v=gp-uuWMwCaI&list=PLa8_mvXW28yKZ86aGYBQkikfmjeJJB0cx
Assessment: Students will be expected to respond to a random subset of the questions included in Appendix A: Online Learning Checks.
Technical Block 11: Marketing, Sales, and Business Development

Description:  Familiarization with solar project development processes and markets, sales methods, and structure of solar industry organizations.
Objectives:
- Fundamental understanding of the roles of marketing, sales, and business development
- Understand types of ownership models for PV systems
- Familiar with types of business development methods used by solar companies

Resources:
Chapter 3 - Site Surveys and Preplanning
NABCEP PV Technical Sales Resource Guide
OnGrid Solar (http://www.ongrid.net/classes_schedule)
Invite business development manager from local PV installation company to make presentation on PV marketing / sales process
Have students obtain PV system quotes for their own homes from different PV companies.
Assessment – Students obtain PV system quotes for their own homes from different PV companies.  Quiz questions included in appendix.


Technical Block 12: Financial Analysis and Tools

Description:  This section of the course focuses on financial strategies that are utilized in the solar industry including direct purchase, self-financed, lease, and power-purchase agreement (PPA), and community / cooperative solar projects.  The types and sources of variable incentive programs are also presented, including local, utility, state, and federal incentives as applied to private individuals, businesses, and organizations.
Objectives
- Understand the role of financing in the solar industry and primary alternative pathways for financing e.g. self, lease, PPA
- Experience using financial analysis tools to evaluate feasibility of solar and support sales and how incentives can encourage investment in PV
Resources:  	http://www.dsireusa.org/
PVFAST Excel tool
Moodle Content description
Assessment – Quiz questions included in Appendix

Technical Block 13: Math Refresher

Description:  Typical math topics required for calculations are included as needed throughout the class through either lectures, tutoring, or through online remedial math tools.
Objectives
- Ability to solve for variables using basic algebra (Ohms Law)
- Ability to solve right triangles for roof slope and area analysis
- Ability to convert SAE to metric units
- Ability to make module temperature correction calculations
- Ability to conduct conductor voltage drop calculations
- Ability to complete derating calculations
Resources:  	Dunlop book
		Chapter 5 – Cell, Modules and Arrays (module temperature correction)
Chapter 11 – Electrical Integration (voltage drop calculations and derating calculations)

Websites
http://khanacademy.org  (Basic mathematics)
Assessment – Assessments for this block are limited to individual remedial math activities recommended by instructor for specific students as needed.

Technical Block 14: NABCEP Exam Review and Completion

Description This aspect of the course supports the successful completion of the targeted exam / credential used in the class.  The objectives are achieved by providing repetitive practice in the administration of quizzes, a mid-term examination, and practice examination using questions that are aligned with the learning objectives and task analysis of the targeted credential.  An important aspect of this element of the course is the use of practice environments (paper or online) that are aligned with the method the actual examination is administered.
Objectives
- Experience in responding to multiple choice and fill-in-the-blank type questions aligned with the credentials and learning objectives of the course
- Confident in the management of time and exam-taking skills
Resources:  	Online Quiz Question Database
		Manual quiz questions (appendix and online)
		Mid Term examination
PV Entry Level Practice Exams
		NABCEP PV Installation Professional Resource Guide (practice questions relevant
to Entry Level learning objectives only)

Assessment – All elements of the course assessments are designed to achieve this learning objective


Activity 1: Site Survey and Analysis

Description: Teams of students are provided an opportunity to gain experience with the processes and tools used to examine a prospective location for a PV array.  This group of activities reinforces solar energy fundamentals in terms of the path of the sun and sun angles, and also provides students with the opportunity to examine and calculate the estimated solar exposure on a given location accounting for shading.
Note to Instructors: The results of the student’s work in this Activity can be used in several other Activities.
Objectives:
- Demonstrate the ability to locate true south, accounting for magnetic declination.
- Identify factors to consider when choosing array locations, including shading, accessibility, structural concerns, and existing electrical equipment.
- Demonstrate the use of equipment and software tools to evaluate solar window obstructions and shading at given locations, and quantify the reduction in solar energy received.
- Understand the consequences of array shading and best practices for minimizing shading and preserving array output.
- Identify customer concerns and site issues that may arise during a preliminary site assessment.
- Gain experience with Solar Pathfinder and Solmetric SunEye, as well as navigating a physical space while considering the solar window.
Resources:
- Unit 1.2 – Solar Radiation, Module 1: Knowing the Sun Path from Penn State’s PV Design and Construction course.
- Unit 2.1 – Array Positioning and Site Evaluation from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 4, Solar Site Analysis and Mounting.
- Dunlop, James. “Photovoltaic Systems” Chapter 2 – Solar Radiation.
- Dunlop, James. “Photovoltaic Systems” Chapter 3 – Site Surveys and Preplanning.
- “Developing Site Plan Drawings.” SwitchBoard – From the Forum. Solar Professional Magazine. June/July 2014, p.66.
- “How to Use a Solar Pathfinder.” https://www.youtube.com/watch?v=sXKGGW8-7dM
- Galli, Mark. Hoberg, Peter. “Solar Site Evaluation.” Solar Professional Magazine. Dec/Jan 2009. https://solarprofessional.com/articles/design-installation/solar-site-evaluation?v=disable_pagination
- Tobe, Jeff. “Successful PV Site Evaluation.” Home Power Magazine. Dec/Jan 2014. http://www.homepower.com/articles/solar-electricity/design-installation/successful-pv-site-evaluation?v=print
Worksheet: Activity 1 – Site Survey and Analysis.docx
Assessment: Students are assessed based on their participation in the activity and demonstration of competence in the use of the Solar Pathfinder in addition to related topics covered in quizzes. Student’s results can be compared between groups and  standardized into a “real” solution, or compared to a professional analysis or 3D CAD model (with shading of the space.

Activity 2: Module Characterization

Description: This activity enables students to experiment with a solar module in daylight or artificial light sufficient to generate voltage and current.  Output of module is measured and IV curve traced using testing tools and compared to expected results based on name plate data.
Objectives:
- Demonstrate the safe measurement of voltage and current of individual PV modules.
- Predict the effect of module tilt and orientation on open circuit voltage and short circuit current.
- Summarize the effects of shading on single cells, rows, and columns of a solar module on the electrical characteristics of the modules.
-
Resources:
- Sanchez, Justine. “The Circuit – Methods: Testing and Verifying Module Voltage & Current…Prior to Installation.” Home Power Magazine. April/May 2010. http://www.homepower.com/circuit-methods-testing
- PVEducation.org “Shading” http://www.pveducation.org/pvcdrom/modules/shading
- Unit 1.3 – PV Cells, Modules, and Arrays  from Penn State’s PV Design and Construction course.
- Unit 2.2 – Array Positioning and Site Evaluation, Module 4: Shading from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 3, Modules, Series and Parallel, and Meters.
- Dunlop, James. “Photovoltaic Systems” Chapter 5 – Cells, Modules, and Arrays.
Worksheet: Activity 2 – Module Characterization.docx
Assessment: Students are assessed based on their participation in the activity and demonstration of competence safely using electrical testing equipment in addition to related topics covered in quizzes.

Activity 3: PV System Components

Description: This activity provides students to see actual PV system components in live systems and or in training system settings. The objective are accomplished by providing a tour of equipment and systems and providing students a chance to see and touch various types of systems and components.  Removal of cover plates of non-active equipment is required, along with time taken to point our electronic components in devices.  This activity can also be combined with staging activities necessary to prepare for hands-on activities to follow.
Note to Instructor: This Activity includes references to electrical and structural drawings, including  single line, 3 line, or as-built drawing of the array at hand. If available, these resources can serve as an introduction to the ‘language’ of plan sets, and give the student practice orienting themselves electrically and physically in relation to a PV system.
Objective:
- Become familiar with major PV system components, name plate characteristic, including PV modules and arrays, inverters and chargers, charge controllers, combiner boxes, disconnects batteries and other sources.
- See examples of how basic electronic devices are used in PV systems (transformers, diodes, etc.)
Resources:
- Unit 3.1 – Introduction to the NEC and PV System Components from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 2, Electricity and Components.
- Dunlop, James. “Photovoltaic Systems” Chapter 4 – System Components and Configurations.
- Dunlop, James. “Photovoltaic Systems” Chapter 8 – Inverters.
-

Assessment: Students are assessed based on their participation in the activity in addition to related topics covered in quizzes.

Activity 4: PV System Exploration

Description: This activity provides students in small groups with experience gathering inverter and PV modules name-place information from an existing system or systems or from inverters in a classroom setting.  Students are asked to calculate an estimate of the maximum and minimum modules that could be paired with the inverter.  Ideally teams are distributed across variable types of systems including 600V and 1000V inverters and micro inverters.
Note to Instructor: Depending on how this topic has been covered up to this point, the instructor may choose to walk through each step in the worksheet with a given inverter and ask student s to replicate that step with other inverters.
Objectives:
- Locate and assess the specific attributes of photovoltaic inverters.
- Calculated the maximum and minimum string size for  given modules and inverters.
- Understand rational for over-sizing strings of modules and concept of inverter clipping.
Resources:
- Unit 3.2 – Electrical Integration and Code Compliance from Penn State’s PV Design and Construction course.
- Unit 4.1 – Grid-Tied Systems Without Batteries from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 5, Grid-Direct System Sizing.
- Dunlop, James. “Photovoltaic Systems” Chapter 4 – System Components and Configurations.
- Dunlop, James. “Photovoltaic Systems” Chapter 5 – Cells, Modules, and Arrays.
- Dunlop, James. “Photovoltaic Systems” Chapter 8 – Inverters.
- Dunlop, James. “Photovoltaic Systems” Chapter 10 – Mechanical Integration.
- Dunlop, James. “Photovoltaic Systems” Chapter 11 – Electrical Integration.
Assessment: Students are assessed based on their participation in the activity, the correctness of their answers to the worksheet, and their demonstration of key concepts in a team presentation of findings for the system their team examined in addition to related topics covered in quizzes.

Activity 5: System Assembly and Building Integration

Description: This activity enables students to gain experience in the assembly of PV systems with variable types of mounting, racking, and inverter configurations.  Four-Five separate systems are assembled simultaneously by small groups, with some rotations of groups enabled to offer students the chance to work with different types of equipment.  Desirable attributes to include in systems:  grounded and ungrounded inverters, 600Vac and 1000Vac inverters, micro inverters, sloped roof installation, flat-roof installation, single string, and multiple string systems.
Note to Instructor: If possible, plan sets reflecting physical layout for any training equipment should be made available to the student so that the student can gain experience placing an array and its individual components according to a defined, code compliant plan and within the constraints of a specific site.
Objectives:
- Describe the main factors to consider in the integration and mounting of arrays and other equipment.
- Identify the common ways PV arrays are mechanically secured and installed on the ground, to building rooftops or other structures, including rack mounts, ballasted systems, pole mounts, integral, direct and stand-off roof mounts, sun tracking mounts and for other building-integrated applications.
- Compare and contrast the features and benefits of different PV array mounting systems and practices, including their design and materials, standardization and appearance, applications and installation requirements, thermal and energy performance, safety and reliability, accessibility and maintenance, costs and other factors.
- Determine the available space for an array, taking module size, module and panel spacing, roof or ground support structure and attachment points, and fire and safety limitations into account
- Identify mechanical design features that affect the electrical and thermal performance of PV arrays, including array orientation, mounting methods and other factors.
- Identify PV system mechanical design attributes that affect the installation and maintenance of PV arrays, including hardware standardization, safety and accessibility, and other factors.
- Identify the types and magnitudes of mechanical loads experienced by PV modules, arrays and their support structures, including dead loads, live loads, wind loads, snow loads, seismic loads, in established combinations according to ASCE 7-05 Minimum Design Loads for Buildings and Other Structures.
- Review and recognize the importance of PV equipment manufacturers’ instructions with regard to mounting and installation procedures, the skills and competencies required of installers, and the implications on product safety, performance, code-compliance and warranties.
- Gain experience with the assembly of steep roof, ballasted, and ground mount PV systems including racking assembly, module installation, wiring, grounding, and inverter hook-up and grid interconnection
Resources:
- Activity guides and specification sheets for equipment used
- Training equipment
- Appropriate hand tools
- Appropriate personal protective equipment and fall protection gear

Assessment: Students are evaluated on their participation and engagement in these activities, and in related quiz questions described in technical blocks.

Activity 6: System Commissioning

Description: This activity utilizes training systems assembled by students to present commissioning and start-up procedures based on equipment specifications and safe practices for energizing PV systems.  An emphasis is placed on an understanding of expected and measured performance of systems, and as such, this activity is best conducted in sunny conditions.
Note to Instructor: The Activity described here can be combined, included with, or switched with elements of Activity 9: PV Systems Operations and Maintenance. With the exception of insulation resistance testing (usually done only during commissioning, and not covered in this course) and the initial turn-on procedures for equipment, system commissioning and O&M are very similar. The Activities described here and in Activity 9 reflect that overlap.
Objectives
- Represent the I-V curve of a specific PV module through direct measurement.
- Describe a PV module’s operating point.
- Become familiar with procedures to systematically evaluate the performance of a PV system to ensure proper operation.
- Identify and describe the use and meaning of typical performance parameters monitored in PV systems, including DC and AC voltages, currents and power levels, solar energy collected, the electrical energy produced or consumed, operating temperatures and other data.
- Compare PV system output with expectations based on system sizing, component specifications and actual operating conditions, and understand why actual output may be different than expected.
- Understand basic troubleshooting principles and progression, including recognizing a problem, observing the symptoms, diagnosing the cause and taking corrective actions leading from the system, subsystem to the component level.
- Gain experience using diagnostic equipment to evaluate the performance of PV systems.
Resources:
- Unit 5.2 – Commissioning, Maintenance, and Troubleshooting from Penn State’s PV Design and Construction course.
- Penn State Solar Operations and Maintenance video series - https://www.youtube.com/watch?v=gp-uuWMwCaI&list=PLa8_mvXW28yKZ86aGYBQkikfmjeJJB0cx
- Dunlop, James. “Photovoltaic Systems” Chapter 14 - Commissioning, Maintenance, and Troubleshooting.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 7, Safely Installing and Commissioning Grid-Direct Systems.
- FLIR Application Story, “Inspection of Roof-Mounted Solar Panels with Thermal Imaging.”  http://www.flir.com/uploadedFiles/Thermography/MMC/Brochures/T820452/T820452_EN.pdf
- Auditing and module characterization spreadsheets
- Mackey, Andy and United Management and Consultants. “Commissioning & Troubleshooting PV Process Guide.” Powerpoint Presentation.
- United Management and Consultants. “Commissioning and Testing of PV Systems” Powerpoint Presentation
- Example Commissioning Documents
Assessment: Students are evaluated on their participation and engagement in these activities, including their completion of the I-V curve worksheet, and in related quiz questions described in technical blocks.

Activity 7: System Design - String and Inverter Matching

Description: For a specified PV module and inverter in a simple utility-interactive system, students determine the maximum and minimum number of modules that may be used in source circuits and the total number of source circuits that may be used with a specified inverter depending upon the expected range of operating temperatures, the inverter voltage windows for array maximum power point tracking and operation.  Both manual calculations and inverter manufacturers’ online string sizing software tools are used in this activity.  This activity can be conducted in class and/or complimented with online string-inverter matching activities.
Note to Instructors: This activity can be blended with Activities 1 and 2, where the space assessed by the students in Activity 1 and the modules used for Activity 2 can be used as the design basis for the Activity. The attached worksheet assumes this is the case.
Objectives:
- Become familiar with process for evaluating string and inverter matching design
- Understand the factors and variable affecting string-inverter matching
- Adjust the open circuit and maximum power voltages of PV module to reflect high and low temperature extremes at an installation site.
-
- Understand roles and value of manufacturer sizing tools and application engineers
- Cacluate an acceptable string size based on a chosen module and inverter
Resources:
- Unit 3.1 – Introduction to the NEC and PV System Components, Modules 5, 6, and 7 from Penn State’s PV Design and Construction course.
- Unit 3.2 – Electrical Integration and Code Compliance from Penn State’s PV Design and Construction course.
- Unit 4.1 – Grid-Tied Systems Without Batteries from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 5, Grid-Direct System Sizing, and Unit 6, Grounding, Wiring, and Overcurrent Protection.
- Wire sizing, string sizing, orientation, and interrow shading spreadsheets
- Example spec sheets, Sketchup files, and images of board work included for reference.

Worksheet: Activity 7 - System Design - String and Inverter Matching.docx
Assessment: This activity reinforces topics on module characteristics, inverters, and PV systems which are assessed in quiz questions.
Activity 8: System Inspection

Description: This activity enables students to experience the processes and activities required to prepare for and document inspection of PV activities.  Common problems found during inspections, and the value of high quality permit drawings are emphasized.
Objectives:
- Become familiar with processes required for preparing systems for inspection including labeling.
- Understand the safety requirements for inspecting different types of PV systems and related equipment.
- Understand the procedures and tools to support permitting and inspection activities of PV systems
- Highlight the ways in which a system might change between a pre-construction plan review and an as-built system.
Resources:
- Unit 3.1 – Introduction to the NEC and PV System Components from Penn State’s PV Design and Construction course.
- Unit 3.2 Electrical Integration and Code Compliance from Penn State’s PV Design and Construction course.
- Unit 5.1 Safety, Permitting, and Inspection from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 5, Grid-Direct System Sizing, and Unit 6, Grounding, Wiring, and Overcurrent Protection.
- Dunlop, James. “Photovoltaic Systems” Chapter 13 – Permitting and Inspection.
- Mayfield, Ryan. “Common Code Violations: Residential PV Systems.” Solar Professional Magazine. December/January 2010.
- Penn State Inspecting PV video series - https://www.youtube.com/watch?v=APrbl0Ngp8o&list=PLa8_mvXW28yLqZjgEidnbEjBFgD1mFIl9
- Wright, Kevin. “Inspecting PV.” Powerpoint Presentation.
- Wright, Kevin. “Code Training.” Powerpoint Presentation.
- United Management and Consultants. “PV System Checklist.” Excel spreadsheet checklist.
Assessment: This activity reinforces objectives in technical blocks and is assessed through quiz questions.

Activity 9: PV Systems Operations and Maintenance

Description: This activity reinforces learning objectives associated with commissioning systems in the context of maintenance and operations.  An emphasis is placed on common problems with PV systems over time, and on the role of operations and maintenances services in the PV industry, including how decisions about investments and procurement of these services take place.
Note to Instructor: The Activity described here can be combined, included with, or switched with elements of Activity 6: System Commissioning. With the exception of insulation resistance testing (usually done only during commissioning, and not covered in this course) and the initial turn-on procedures for equipment, system commissioning and O&M are very similar. The Activities described here and in Activity 6 reflect that overlap.
Objectives:
- Discuss various potential problems related to PV system design, components, installation, operation or maintenance that may affect the performance and reliability of PV systems.
- Understand the safety requirements for operating and maintaining different types of PV systems and related equipment.
- Identify and describe the use and meaning of typical performance parameters monitored in PV systems, including DC and AC voltages, currents and power levels, solar energy collected, the electrical energy produced or consumed, operating temperatures and other data.
- Compare PV system output with expectations based on system sizing, component specifications and actual operating conditions, and understand why actual output may be different than expected.
- Understand basic troubleshooting principles and progression, including recognizing a problem, observing the symptoms, diagnosing the cause and taking corrective actions leading from the system, subsystem to the component level.
- Identify the most common types of reliability failures (Failure Modes) in PV systems and their causes due to the equipment, quality of installation and other factors.
- Understand how O&M services are procured, and parameters for making decisions about O&M investments
Resources:
- Dunlop, James. “Photovoltaic Systems” Chapter 14 – Commissioning, Maintenance, and Troubleshooting.
- United Management and Consultants.  “PV Systems Operation & Maintenance” Powerpoint Presentation
- Example O&M economic calculations and documents.
- Example plan sets and monitoring data.
- NREL 63235 Best Practices in PV System Operations and Maintenance
- Example array living documents, IR reports, IV reports, monitoring data, and plan sets
- Sanchez, Justine. “Potential PV Problems & New Tools for Troubleshooting.” Home Power Magazine. June and July 2011, p 78.
- Williams, Dave. “Large Scale PV Operations and Maintenance.” Solar Professional Magazine. June/July 2010.
- Unit 5.2 Commissioning, Maintenance, and Trouble-shooting from Penn State’s PV Design and Construction course.
Assessment: This activity reinforces objectives in technical blocks and is assessed through quiz questions.
Activity 10: PV System Financial Analysis

Description:  This activity provides students with an opportunity to use basic financial analysis tools to evaluate the cash flow expectations for PV projects.  An emphasis is placed on the role of financing strategies in the development of PV projects, and the impacts of variable forms of incentives on the return on investment and rate of return.
Objectives:
- Understand the role of financing in the solar industry and primary alternative pathways for financing e.g. self, lease, PPA
- Experience using financial analysis tools to evaluate feasibility of solar and support sales and how incentives can encourage investment in PV
- Understand how a system’s mechanical and electrical design can influence the final installed cost of a PV system and the payback period.
- Understand how taxes, depreciation, SRECs, and other financial tools and techniques can influence the final installed cost of a PV system and the payback period.
Resources:
- The Database of State Incentives for Renewables and Efficiency. http://www.dsireusa.org/
- PVFAST finance tool examples
- United Management and Consultants. “PV System Financing and Economics” Powerpoint Presentation
- Unit 5.3 Metering and Interconnection Agreements from Penn State’s PV Design and Construction course.
- Unit 5.4 Economic Analysis and Financing from Penn State’s PV Design and Construction course.
- Solar Energy International, “Solar Electric Handbook: Photovoltaic Fundamentals and Applications.” Unit 9, Solar Business and Finance.
- Dunlop, James. “Photovoltaic Systems” Chapter 15 – Economic Analysis.

Assessment: Quiz questions included in appendix

Activity 11: Case Study and Site visit:  Armed Forces PV Installation

Description: By learning about a project’s history, from inception through operation, this activity will will provide an opportunity for students to learn more about the variety of design, engineering, construction, and financing found in the modern solar industry. Students will be expected to understand the reasoning behind decisions made during the project’s inception and construction, as well as the implications of those decisions on other parts of the project.
Objectives:
- Gain a comprehensive understanding of a local project (preferably military base) case including project development, site, geo-technical solutions, construction processes, O&M needs, and performance evaluation based on monitoring data
- Gain an understanding of the goals and objectives for solar energy deployment on Armed Forces facilities.
Resources:
- Engineering or construction drawings and proforma documents for the selected project
- Interviews or guest lectures with project owners, engineers, or contractors
- Articles about project in periodicals
- Construction management and O&M professionals to support tour/presentation
Assessment: Discussion questions in class


Transition Solar Block 1: Veterans in the Solar Industry

Description This course component enables veterans to learn about jobs and career experience in the solar industry from their peer.  Video interviews and Skype meetings distributed during class meetings are used to achieve objectives.
Objectives
- Describe the perspective of veterans and their transition out of the military into the solar industry.
- Describe what aspects of solar careers veterans have found rewarding and fulfilling

Resources:  	DOE SRV video
First Solar Video 1
First Solar Video 2
Skype interviews with class (see contact list)
Solar Foundation Report http://www.thesolarfoundation.org/wp-content/uploads/2015/01/VeteransInSolarWEB.pdf

Assessment –Discussion Questions
(Class Discussion) What surprised you about the way other veterans talk about their jobs and careers in the solar industry?
(Class Discussion) What are some attributes about the solar industry that you feel are aligned with the values and traits of veterans? In other words, in what ways do you think veterans might be uniquely suited to work and thrive in the solar industry?

Transition Solar Block 2: Solar Resume Design and Strategy

Description This course component focuses on the development of a 2-page resume that reflects skills and experiences of participants, as well as defined career objectives that are aligned with the opportunities in the solar industry.  This objective is fulfilled through the incremental improvement of resumes with feedback from peers and solar industry / instructors.
Objectives
- Represent individual backgrounds, skills, and education in a solar industry-focused resume and professional development plan.
- Author career objectives customized to specific job and career opportunities

Resources:  	Online SRV placement systems (for example Futures, Inc)
Sample resumes from veterans that have obtained jobs
 Sample objective statements

Assessment –Resume Design
(online) Evaluate the online resources that are available to veterans to support resume development and job placement.  How do you feel these resources can be useful to you?
(online) Summarize the initial feedback you received on your resume and how you will respond to this feedback
Submit a final resume for the package of resumes that will be distributed for your cohort of SRV.

Transition Solar Block 3: Solar Career Mapping

Description This course component provides an opportunity for participants to learn about the variable types of career pathways that exist in the solar industry, and to gain experience with assessment tools that can help participants make connections between their interests, skills, and passions and the job/career opportunities that exist in the solar industry.  A part of this discussion is an in-class activity in which students spend 30 minutes interviewing each other to learn about each other’s’ interests and passions using an active listening approach.
Objectives
- Gain experience using resources that are designed to help identify entry points and pathways in solar design, construction, manufacturing, and project development.
- Gain experience in the use of career and job placement tools and services targeting veterans.
- Gain experience in the self-assessment of core values and contributions.
- Gain experience with Solar Foundation workforce research

Resources:  	DOE Solar Career Map http://energy.gov/eere/sunshot/solar-career-map
Active Listening Interview PPT
Self-Assessment (Futures, Inc or other tool)
 Sample objective statements

Assessment –Career Identification
(Class discussion) Summarize the core qualities and traits you learned about a colleague in your class during your active listening interview.
(Class discussion) What qualities and passions do you possess that might be valuable to a solar industry organization?
(online) Which of the vertical pathways on the Solar Career Map are of greatest interest to you and why?
(online) Select two high level career positions on the map that are of interest to you and outline the background, experience, and credentials for these positions.
(online) Describe some of the driving forces in the solar job market


Transition Solar Block 4: Solar industry organizations and professional associations

Description This course component provides an opportunity for participants to learn about the way solar companies have formed and evolved in recent years, and provides exposure to the trade associations and periodicals that can help them learn about and stay current on key issues in the solar industry.
Objectives
- Describe the driving forces shaping solar companies
- Describe the roles manufacturers play in supporting solar project development
- Describe the mission of SEIA and the services offered by regional chapters of SEIA

Resources:  	Company websites and guest speakers
		Solar Industry Magazine
		Solar Pro Magazine / website

Assessment –Solar industry organizations
(Class discussion) After reviewing multiple website of large and small solar companies that operate in regions you would like to work, how would you describe some of the major differences?
(Class discussion) What types of solar companies, or traits of companies are appealing to you, and why?
(online) Write 2-3 sentences describing your interest in at least two different solar companies that operate in your region.  These paragraphs will be used in cover letters or thank you letters during your job search.

Transition Solar Block 5: Post 9/11 GI Bill

Description This course component helps expose participants to the resources that support education and continuing education for veterans.
Objectives
- Understand purpose and scale of post 9/11 GI Bill resources
- Understand process to apply for Post 9/11 GI Bill resources

Resources:  	DOE SRV Webinar on Post 9/11 GI Bill

Assessment –Post 9/11 GI Bill
 (online) Based on your job and career interests, what types of additional education and training might you pursue, and how would you seek Post 9/11 GI Bill resources to support them?


Transition Solar Block 6: Interview Skills and Job Search

Description This course component provides participants with experience preparing for and participating in interviews for actual jobs and careers in the solar industry.
Objectives
- Confidently participate in both informal and formal interview settings.
- Develop ability to communicate personal strengths and aspirations, understand methods to follow up with opportunities.

Resources:  	Webinar with Solar Recruiting Professional
SRV PPT with interview tips and strategies

Assessment –Interview Skills
 (Discussion) What are the four things that an interviewer is seeking to learn about you?
	Can you do the job?
	Will you take the job?
	Are you a good fit for the job?
(online) What are the top three take-aways you hope an interviewer will learn about you?
Complete at least one online or face to face interview for a job opening in the solar industry.


