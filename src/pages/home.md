![Energy Manager Logo|width=399px,class=image_center](em_logo.png)

# Energy Manager Institute|class=page_title_center

![EMI campus map](campus_map.svg)

##

The Energy Manager Institute is a set of tools and information for teaching basic concepts in energy generation and efficient use, with a focus on renewable energy. Although the specific knowledge is energy specific, the long-term purpose of is to improve the general STEAM skills of middle and high-school students. By providing teachers and students the tools to collect and make use of the data from their school's existing photo-voltaic systems, or to simulate future ones, they will learn real world engineering while helping their school develop and maintain their alternative energy and emergency management systems.

[More details...](./about)

## Features
- STEM projects: energy management, electric cars, solar cooking
- Guides to technology: What is worth using, and how to use it.
- Photovoltaic: What is is? Is it worth it?


## Schools

- [Energy](./energy/intro)
- [Solar](./solar/intro)
- [Projects](./projects/intro)


## Content

### Energy Guide

- [Intro](./energy/intro.md)
- [Power consumption estimation](./energy/energy_use.md)

### Solar Technology Guide
- [Intro](./solar/intro)
- [Solar Fundamentals](./solar/solar_fundamentals)
- [Photovoltaic Modules](./solar/PV)
- [Power Consumption Estimation](./solar/power)
- [System Design](./solar/system_design)
- [Economics](./solar/economics)

### Climate Guide

- Climate-101

### Energy Simulation
- [Introduction](./energy_simulation/energy_simulation#introduction)
  - Minetest mods



#

EMI, a subsidiary of the Florida Solar Energy Center
