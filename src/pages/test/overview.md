# Overview
Welcome to the FSEC installer training. In this first section I’ll give you an introduction to the course

## Learning Objectives
* Logistics
* Expectations from this course

## Contents Slide in Each Chapter

__Course__ ❱ Logistics ❱ Objectives ❱ Expectations

__Solar Industry__ ❱ Value Chain ❱ Organizations ❱ Market

### Interactive


![Utility-Interactive Systems](solar_system_cofiguration_interactive.jpg)

[test2](pvsystem_cofiguration_interactive.jpg)

Manufacturers **➜** Distributors **➜** Installers **➜** Home Owners, Commercial and Industrial, Utilities
