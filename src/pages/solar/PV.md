# Photovoltaic Modules

## Learning Objectives

After completing this chapter, you will be able to properly:

- Describe the photovoltaic effect.
- Explain differences between module types.
- Describe PV manufacturing techniques.
- Recognize the main module components.
- Understand environmental effects on module output.
- Interpret the IV curve.
- Construct an array.
- Describe the effects of dissimilar devices.

## PV cells

### Cell History

- 1839: Edmund Becquerel - Discovery
- 1883: Charles Fritts - First Solar Cell
- 1905: Albert Einstein - Described Effect
- 1954: Bell Laboratories - First Silicon Cell
- 1960s: Space Industry - Powering Satellites

![Explorer 6 with solar paddels up. https://en.wikipedia.org/wiki/File:Explorer_6_paddles_up.jpg](Explorer_6_paddles_up.jpg)

- 1973: Solar One - Solar-Powered Residence
- 1974: Oil Crisis - Terrestrial Applications
- 1981: Solar Challenger - First aircraft to run on solar power

![Solar Challenger drawing https://commons.wikimedia.org/wiki/File:Solar_Challenger_drawing.jpg](Solar_Challenger_drawing.jpg)

- 1979: Solar White House - Solar panels installed on the White House

- 1980-2020: Photovoltaic technology improves, prices go down. Solar becomes practical to install on normal buildings.


### About Photovoltaics

A solar cell, or photovoltaic cell, is an electrical device that converts the energy of light directly into electricity by the photovoltaic effect, which is a physical and chemical phenomenon. Individual solar cell devices can be combined to form modules, otherwise known as solar panels.

![Solar Cell](Solar_cell_grey.png)

- Photons in sunlight hit the solar panel and are absorbed by semiconducting materials, such as silicon.
- Electrons are excited from their current molecular/atomic orbital, and some travel through the cell until it reaches an electrode.
- Current flows through the material to cancel the potential and this electricity is captured.
- An array of solar cells converts solar energy into a usable amount of direct current (DC) electricity.

![Solar Cell](From_a_solar_cell_to_a_PV_system.svg.png)


### Photovoltaic Effect


## PV Types

- Crystalline and Polycrystalline:
  - Time Tested Technology
  - In use for 40+ years
  - Majority of Current Market

- Thin-Film:
  - Opportunities for cost reduction
  - Typically not as efficient
  - Variety of materials
  - Market share growing

### Crystalline Rooftop Application

Advantages:
- Higher efficiency, less area required which can lead to lower Balance of Systems cost
- Technology more mature, more reliability information available

Crystalline and Polycrystalline:
- Time Tested Technology
- In use for 40+ years
- Majority of Current Market

### Thin-Film Rooftop Application

Advantages:
- Lower cost per watt, typically
- Opportunity to reduce installation cost due to cheaper mounting methods
- Better temperature coefficients, potentially more kWh per rated watt

Thin-Film:
- Opportunities for cost reduction
- Typically not as efficient
- Variety of materials
- Market share growing

### CIGS (Copper Indium Gallium Diselenide)


## Module

### Construction

Cells wired in series to build voltage

PV cells are usually connected in series within a module.  This means the voltage from each cell is added together and the current remains the same.

Example:  Each cell generates 0.7 volts and 9 amps.  This series generates 4 x 0.7V = 2.8V and 9 amps.

Cells wired in parallel to build current


PV cells are usually connected in series within a module.  This means the voltage from each cell is added together and the current remains the same.

Example:  Each cell generates 0.6 volts and 4 amps.  This series generates 4 x 0.6V = 2.4V and 4 amps.



### Wiring Boxes

Enclosed terminals allow module connection to other devices

Includes bypass diodes

99%+ have sealed “potted blocks,” not junction boxes



### Bypass Diodes

Figure 5-30 P.140

On the back of the module sits the Junction Box.  This connects the electronics inside the PV module to the electronics outside the PV module.  The junction box also contains the bypass diodes for the module.  These diodes allow current to efficiently flow through the PV module even if some of the cells are shaded and causing a resistance.



### I-V Curves

![I-V Curve example](IV_curve.svg)

### Six Electrical Parameters

- Max Power
- Short-Circuit Current
- Operating Current
- Operating Voltage
- Open-Circuit
- Voltage

### UL Listing Important

Maximum Permissible System Voltage
Label required by NEC 690.51


### Temperature & Voltage

- Higher Voltage in Cold
- Lower Voltage in Heat

- Temperature: 25°C
  - Open Circuit Voltage: 20.1V
- Temperature: 65°C
  - Open Circuit Voltage: 17.4V

Look at Figure 5-20 to see how Voltage affects IV curve
Look to handout for example to work through.

Don't forget that a temperature increase affects not just voltage, but power!





### Test conditions

- Name
- Irradiance
- Cell
- Temperature
- Ambient
- Temperature
- Wind Speed
- Air Mass

### Standardized test conditions

- STC (Standard Test Conditions)
  - 1000 W/m2
  - 25°C (77°F)

- NOCT (Nominal Operating Cell Temperature)
  - 800 W/m2
  - 45°C-50°C (113°F-122°F). This is different for each cell product


- STC: Required by NEC, only realistic in very cold climates (cell temp normally higher)
- SOC: More realistic than STC with lower peak power estimates.
- PTC: Similar to SOC but cell temp is higher.
- NOCT: This is different for each cell product



### Solar PV Module Efficiency

Efficiency is the ratio of the output power to the input power.

Efficiency = Output Power/Input Power * 100

Since modules are normally priced by $/Watt, the efficiency mostly effects the __area__ of the module, and therefor how many panels can fit in a given location.

### Equipment Standards

- UL 1703: Standard for Safety for Flat-Plate Photovoltaic Modules and Panels
  - Required for all PV systems in the U.S.
  - Focus: Safety
  - UL Website:  www.ul.com
- IEC 61215 (crystalline) and IEC 61646 (thin film): Design Qualification of Type Approval of Photovoltaic (PV) Modules
  - International standard (not required for U.S.)
  - Focus: Reliability and Durability

- “UL approved” means tested to UL standards, NOT that Underwriters Laboratories needs to test.
- NRTLs (Nationally-Recognized Test Laboratories) may test to UL standards and approve products




### Qualification Tests

- Thermal cycling tests
- Humidity - freezing tests
- Impact and shock tests
- Immersion tests
- Cyclic pressure, twisting, vibration and other mechanical loading tests
- Wet/dry hi-pot, excessive and reverse current electrical tests
- Other electrical and mechanical tests





## Array

### Building Array


### Derating Factors

- System Availability
- Soiling
- AC Wiring
- DC Wiring
- Diodes and Connections
- Mismatch
- Inverter and Transformer
- PV Module Nameplate DC Rating
- 85%+ Saved

Not All Efficiency is the same. This is not all efficiency from the sun.




## Summary

- What is the photovoltaic effect?
- What are the differences between types of PV modules?
- How is PV manufactured?
- What are the main components of a PV module?
- What are the effects of sunlight and temperature on module performance?
- What does an I-V curve represent?
- How do you electrically assemble an array from modules?
- How do dissimilar modules interact when wired in series/parallel?
- How do derating factors describe total array performance?
