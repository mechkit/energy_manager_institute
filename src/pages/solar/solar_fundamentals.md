# Solar Fundamentals

## Learning Objectives

After completing this chapter, you will be able to properly:
* Describe Basic Solar Terminology
* Understand the Difference between Solar Irradiance and Insolation
* Diagram the Sun’s Path across the Sky Throughout the Year
* Determine Proper Array Orientation
* Understand Solar Radiation Data

## SOLAR power

### Solar Radiation

* Even over a vast distance, an enormous amount of energy reaches Earth from the sun.
* The Earth continually receives about 170 million GW of power from the sun

![Solar Radiation from space to Earth](space-atmo.png)


### Irradiance (Power/Area)

Irradiance is a measure of the Sun’s INTENSITY

* Measured as power per unit area (W/m2)
* Varies with distance from sun
* It is an INSTANTANEOUS value

Reference: [Inverse square law](http://hyperphysics.phy-astr.gsu.edu/hbase/forces/isq.html)

### Solar Constant

The “Solar Constant” (Ec) is the intensity of the Sun’s radiation at the edge of our atmosphere (in space)

Ec averages 1,366 W/m2

### Atmosphere

The sun’s energy has to pass through obstacles before reaching PV modules at the Earth’s surface:

* Earth’s atmosphere
* Clouds, rain, humidity
* Dust and pollution
* Latitudinal “thickness” differences

### Atmosphere Effects

* Direct Radiation
* Diffuse Radiation
* Albedo

![Atmospheric Effects](PV_fundamentals_Atmospheric_Effects.png)

### Peak Sun

![Peak Sun](PV_fundamentals_Peak_Sun.png)

* Around solar noon
* Clear day
* Module or array perpendicular to the suns rays
* Irradiance = 1000 W/m2

### Irradiance

![Irradiance](PV_fundamentals_Irradiance.png)

* Obstructions impact Solar Irradiance
* Irradiance fluctuates throughout the day


### Solar Irradiance Measurement

![Solar_Meter](PV_fundamentals_Solar_Meter.png)

* Inexpensive Solar Meter

### Solar Insolation (Energy/Area)

* Solar Insolation is a measurement of irradiance over time
* Represents an amount of energy
* Measured in kWh/m2 or __Peak Sun-Hours__
* Also called Irradiation

Remember:

    Power   →  W, or kW
    Energy  →  Wh, or kWh


### Insolation

![Insolation](PV_fundamentals_Insolation.png)
![Insolation 2](PV_fundamentals_Insolation_2.png)

### Solar Insolation (Peak Sun Hours)

![Peak sun hours 1](PV_fundamentals_Peak_sun_hours_1.png)
![Peak sun hours 2](PV_fundamentals_Peak_sun_hours_2.png)
![Peak sun hours 3](PV_fundamentals_Peak_sun_hours_3.png)
![Peak sun hours 4](PV_fundamentals_Peak_sun_hours_4.png)

### Solar Insolation and PV

Solar insolation data is used to calculate the energy output of an array.

Example: PV array produces 3 kW AC at peak sun.  How many kWh will it produce if it receives 5 sun-hours?

    3 kW AC x 5 Sun-hours = 15 kWh AC


![Solar Insolation and PV](PV_fundamentals_Solar_Insolation_and_PV.png)
![Global Map of Photovoltaic Power Potential |https://en.wikipedia.org/wiki/File:Global_Map_of_Photovoltaic_Power_Potential.png](Global_Map_of_Photovoltaic_Power_Potential.png)

### Solar Radiation Data

[![NREL NSRDB viewer](NREL_NSRDB_viewer.png)](https://maps.nrel.gov/nsrdb-viewer/)

* National Renewable Energy Laboratory (NREL)
  * Solar radiation data for many U.S. locations
  * TMY = Typical Meteorological Year
  * Various tilt angles
  * South facing and tracking
  * Climate Conditions

### Sun Path

Solar radiation varies with:
* Time of day (Earth’s daily rotation)
* Seasons (Earths annual rotation around the sun)
* Latitude (Location on Earth’s surface)
* Climatic Conditions (weather, etc.)
* Elevation

### Atmosphere and Time of Day

![Atmosphere and Time of Day](PV_fundamentals_Atmosphere_and_Time_of_Day.png)

### Seasons

![Seasons](PV_fundamentals_Seasons.png)


### Seasons (and Latitude)
Summer (northern hemisphere)
Winter (northern hemisphere)

![Seasons and Latitude](PV_fundamentals_Seasons_and_Latitude.png)

### Sun Path - Seasons

![Sun Path - Seasons](PV_fundamentals_Sun_Path-Seasons.png)
![Sun Path - Seasons](PV_fundamentals_Sun_Path-Seasons_2.png)
