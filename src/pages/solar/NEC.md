## National Electrical Code (NEC)

* Authority for most electrical installations
* Purpose is *practical safeguarding*, ensuring that systems are safely installed to protect people and property.
* Safety standard, not a design guide
* 9 Chapters, many “articles,” from 90-830, plus tables and appendices
* Chapter 6
  * Article 690
    * Article 690, Part V
      * Section 690.41: referenced in class as [690.41]

## NEC Outline

* Article 90: Intro
* Chapter 1: General
* Chapter 2: Wiring and Protection
* Chapter 3: Wiring Methods and Materials
* Chapter 4: Equipment for General Use
* Chapter 5: Special Occupancies
* Chapter 6: Special Equipment
  * Article 690: Solar Photovoltaic (PV) Systems (14 pages out of 822!)
* Chapter 7: Special Conditions
* Chapter 8: Communications Systems
* Chapter 9: Tables—Conductor and Raceway Specs
* Annexes (Appendices)

Note: Article 690 is a very, very, very small part of the NEC. To be an expert you will need to be familiar with lots of extra information in the NEC!


## NEC Outline

* Article 690: Solar Photovoltaic (PV) Systems
  * Part I: General
  * Part II: Circuit Requirements
  * Part III: Disconnecting Means
  * Part IV: Wiring Methods
  * Part V: Grounding
  * Part VI: Marking
  * Part VII: Connection to Other Sources
  * Part VIII: Storage Batteries
* Article 705: Interconnected Electrical Power Production Sources
  * Part I: General
  * Part II: Utility-Interactive Inverters

## NEC Outline

* Chapters 1-4
  * General Requirements for All Systems
* Chapters 5-7
  * Specialized Requirements for Some Systems
* Chapter 8
  * All-inclusive comm. systems requirements
* Chapter 9:
  * Tables for raceway sizes, fill, bends, voltage drop
* Annexes:
  * For information only, not enforceable


Note:
Chapters 1-4- generally apply to all projects
Chapters 5-7: can modify (and overrule) requirements of chapters 1-4.
Chapter 8: Not relevant


## NEC Sections’ Applicability to PV

* 100
* 110
* 200
* 230
* 240
* 250
* 300
* 310
* 312
* 314
* 330
* 338
* 342
* 344
* 348
* 350
* 352
* 356
* 358
* 376
* 392
* 400
* 404
* 408
* 480
* 690
* 705

Source: Mike Holt’s Illustrated Guide to Understanding 2011 NEC Requirements for Solar Photovoltaic Systems

## NEC Revised Every Three Years

![NEC adoption by state](PV_industry_NEC_revised.png)

Source: NEMA.org

Note: Some states slower to adopt than others, and NEC adoption may vary from county to county in some areas! States and local jurisdictions may also take exception to certain pieces of the NEC, so proper prior planning is critical in ensuring you only have to do the job once.


