# Planning and Conducting a Site Survey


## Tasks for a PV Installer
![](tasks_for_installer.png)
* Conducting a Site Survey
* Selecting a System
* System Checkout & Inspection
* Installing the System

## Objectives: Section 2
* Given site survey results and customer expectations, **participants will diagram possible layouts and locations** for a PV array and other system components.



## Objectives (cont.): Section 2
* Identify typical tools and equipment required for conducting site surveys.
* Establish a suitable location with proper orientation for installing a PV array.
* Establish suitable locations for installing inverters, batteries and other balance-of-system components.
* Interpret solar radiation data.
* Estimate and measure peak load demand and average daily energy use.
* Identify site-specific safety hazards.



## Tasks for a Site Survey
* Put PV Here
* Solar Resource
* Assessment
* and
* Energy Estimation
* System Placement & Shading
* Permits, Utility Interconnection Agreement, Rebates


## The Watts’ Example
* Meg A. Watt, a resident of New Smyrna Beach, FL, has called your office and expressed an interest in installing a photovoltaic (PV) solar system on her house.



## Watts’ Example: Reasons for Interest
* Her daughter, Milli, attends an elementary school with a PV system; she has convinced her mother that they need one too.



## Watts’ Example: Reasons for Interest
* Because of the recent rash of hurricanes striking Florida’s coastline, Meg is interested in having power when the utility is down.



## Site Survey
* Based on your conversation (including finances), you both decide to pursue this further.  You set a time to conduct a Site Survey at their home.



## Customer Expectations
* During initial conversations with customer:
  * Are their goals realistic?
  * Get basic information on the site (Google Earth?)




## General Information from the Watts
* Information to collect before going to site survey.


## Utility and Permitting Information
* Utility Information: Get from the customer’s power bill
* Permitting Authority: Usually either the city or the county
* Home Owners’ Association: Can be more restrictive than the permitting authority



## Site Survey Checklist
* General Information
* Roof Assessment
* Inverter, Utility Access, Batteries, and Engine-Generator
* Recommendation
* Site Inspector Information
* Sketch of Roof Area and Proposed Array Location



## Things to Take With You
* Name and Address of Client (and Directions)
* Camera
* Tape measure
* Ladder
* Solar pathfinder or equivalent
* Site survey sheets
* Clipboard
* Pencils and ruler for drawing
* Inclinometer
* Compass



## DSIRE Web Page
* Database of State Incentives for Renewable Energy (DSIRE):  www.dsireusa.org



## Initial Site Assessment
* Assess whether the given site has a suitable location for installing a PV array (roof or ground mounted – initial eyeball, no shading, south facing, enough area?).



## Information on the Roof


## Watts Site Survey
* 1.01 Building Type
* This is a residential home.

* 1.02 Roof Type:
* Shingle




## 1.02 Roof Types
* Asphalt Shingle
* Concrete Tile
* Metal
* Built-up Roof
* Note: Each may require a different method or set of hardware for array installation



## Asphalt Shingle Roof

Notes:
* By far the most common type of roof installed.


## Tile Roof

Notes:
* A more expensive roof that will require more work to install an array.  Concrete tiles are brittle and may crack while working on the roof.  Expect that several will need to be replaced after completing an installation.


## Metal Roof

Notes:
* Metal roofs are becoming more and more popular.  Installations on metal roofs are at least as easy as installing on shingles.  However, they can be a little slippery.

* Flat or low-angle roofs:  Modules need to be installed at an angle
* Requires more equipment to ensure a good weather seal
* Built-Up Roofs

Notes:
* Can be very difficult to attach to the roof surface.  May want to consider ballast system.


## 1.03  Roof or Surface Mounting Condition
* Some roofs may not last their expected lifetime due to a harsh environment or a poor initial installation.
* There are signs that indicate a roof is ready to be replaced.



## Older Asphalt Shingles
* Look for the following:
* brittleness
* cracks
* curling - excessive thermal stress
* mineral-granule coating worn away - this coating protects the shingle from the elements




## Watts Site Survey
* 1.03 Roof Condition
* Unless there are clear signs that the roof is in very poor condition, you usually need to get on the roof (be safe!).  After climbing on the roof and assessing the shingles, you can see there is some wear.  While the shingles are not in perfect condition, they look to be in fair condition.



* Roof Age:  Around 13 years old
* Slight Curling
* Older Asphalt Shingles

Notes:
* Even though the roof was old, we decided to go forward with the installation because the home owners were so enthusiastic.  As long as the customer understands that the array must be removed and re-installed when the roof is replaced, there will be no hard feelings between you and your customers.


## Concrete Tile and Metal Roofs
* Concrete tile look for:
* Cracks, flaking, or chalky deposits around the edges

* Metal roofs look for:
* corrosion in several places




## Older Metal Roof


## 1.04 Roof Age
* Older homes may have roofs that are ready to be replaced.
* If the roof needs to be replaced in only a few years, it is a good idea to have the customer replace the roof before installing the PV array.



## Watts Site Survey
* 1.04 Roof Age
* Ms. Watt was not sure when the roof was last replaced

* Shingles have slight curling

* Estimate 10-15 years old

* Consider replacing roof




## Watts Site Survey
* 1.05 Supporting Structure
* While the shingles do have some wear, the overall supporting structure seems perfectly fine.
* The roof deck is not sagging anywhere.
* Trusses in the attic look fine
* No pre-existing roof leaks
* Step carefully!




## 1.06 Roof or Mounting Surface Slope
* The angle between the roof and the horizontal plane
* Can be measured in degrees:  20º, 30º, 45º
*  Rise over run:  2/12, 3/12, 4/12
* 2/12  roof rises 2 feet for every 12 feet of horizontal distance
* Can be determined with a ruler or an inclinometer



Notes:
* Show an inclinometer here.


## Roof or Mounting Surface Slope
* If the PV array is installed in the same plane as the roof, then the roof pitch (angle) will influence how much energy the array will produce.
* For a south-facing roof, the optimum tilt angle for maximum annual energy collection is slightly less than the site’s latitude.


* 30º
* 45º
* 15º
* More energy during the summer
* More energy over the entire year
* More energy during the winter
* Roof or Mounting Surface Slope
* Example:
*  Daytona Beach: Latitude around 30º




## Watts Site Survey
* 1.06 Roof Surface Slope
* After placing the inclinometer evenly on one shingle, it indicates an tilt angle of around 18 degrees, or about a 4/12 roof slope.


* 4
* 12
* tan(18º) ~ 4/12

* High pitch roof, easy access to attic
* Low pitch roof, attach array to studs from roof surface
* Roof or Mounting Surface Slope
* Strongly recommend attaching the array to the trusses



## Watts Site Survey
* 1.07 Area (sq. feet)
* Azimuth Direction - The roof deck facing the driveway is 4 degrees west of true south, almost perfect.
* Roof Area – The dimensions of the south facing roof were 17’ x 37’ = 629 ft2
* Eave Height – 8 ft
* Ridge Height – 13.5 ft




## Azimuth Direction
* The angle between true South and direction the roof faces
* True South is the optimum direction for energy production
* Determine direction using a compass
* The difference between true south and magnetic south
* At FSEC, magnetic south is about five degrees east of true south
* Charts are available for other locations




## Azimuth Direction
* http://www.ngdc.noaa.gov/geomagmodels/Declination.jsp


## Array Orientation
* Not critical


## 1.08 Accessibility to Proposed Array Location
* Things that can affect accessibility
* Steep roof pitch
* Slippery roof
* High roof eaves
* Obstructions
* Utility lines




## Watts Site Survey
* 1.08 Accessibility to Proposed Array location
* There was no problem getting a ladder in place to easily get to the roof.  This is important so that lifting the array to the roof will not be problematic.




## 1.09 Area Suitable for the Array
* Objective:  Determine where to put the array on the roof so that it will not be shaded from the sun between 9 am and 3 pm (solar time).
* We will use a Solar Pathfinder in this class to make these determinations.



## Watts Site Survey
* 1.09 Area Suitable for the Array
* After using your Solar Pathfinder, you discover that a palm tree located on the West side of the home will shade the roof between 9 am and 3 pm.  Using a tape measure and chalk lines, you identify this portion of the roof and record it as part of your drawing.




## Sketch of Watts’ Home


## Roof Area
* To install 100 W of crystalline PV modules, there needs to be at least 1 m2  (or around 11 ft2) of roof space. This assumes
* The modules have an operational efficiency of 10%
* The modules blanket the entire area of the installation

* Most residential PV systems are somewhere between one to four kW (1000W - 4000W).
* This would require at least 110 ft2 to 440 ft2 of roof space.
* Mounting hardware and array accessibility can take additional roof space.



## Earth’s Orbit Around the Sun
* 1
* 2
* 3
* 4


## Sun Paths for 30o N Latitude
* 1
* 2
* 4
* 3


## Sun Position - Definitions


## Solar Pathfinder

* Using the Solar Pathfinder at the lower west side of the roof
* Solar Pathfinder

* Obstruction from palm tree
* Solar Pathfinder

* Small tree at the base of the roof
* Potential Obstructions


## Shading from Oak Tree


## Trees Grow!
* Same tree four years later!



## Inverter and Battery Access


## Watts Site Survey
* 2. Inverter/Utility/Battery Access
* 2.01 Utility Service
* This is a standard residential 200A, 240V, single-phase utility service.

* 2.02 Proposed Inverter Location
* The electrical service panel is located in the back of the garage.  There is more than enough space to the left of the panel to install an inverter.

* 2.03 Accessibility to Proposed Inverter Location
* The garage is deep enough so that there will be plenty of space between any car parked in the garage and the inverter.




## Watts Site Survey
* 2.04 Proposed Battery Location
* Garage

* 2.05 Accessibility to Proposed Battery Location
* NEC 110.26 specifies a working depth of three feet for servicing energized equipment under 150 V
* There is space for four 100 Ah batteries
* Requirements for ventilation may need to be discussed with the local building code official.

* 2.06 Proposed Engine Generator Location
* None.




## Inverter/Battery Location
* Inverter/Battery Location


## 2.01 Utility Service
* Voltage - Determines what inverters are allowable
* Current Rating - May limit the maximum size of the PV system



## 2.01 Utility Service
* NEC 690.64 (B) (2) allows circuit breaker protecting the service panel bus bar to be 20% of the rating of the bus bar in residential settings.
* Example:
* A 200 amp service panel in a residence may have up to a 40 amp breaker protecting the conductor feeding service panel.
* 20% of 200 A = 40 Amps




## 2.01 Utility Service
* For a commercial system, the a tap on the supply side of the main breaker may be required.



## 2.02 Proposed Inverter Location
* What to look for
* Accessible Location
* Inverters make noise, so place away from living areas
* Outdoor location is an option for many inverters (not in direct sunlight)



* Inverter
* Location
* AC
* Panel
* 2.02, 2.03 Proposed Accessible Inverter Location


## 2.04 Proposed Battery Location
* Batteries are dangerous.  They contain lead (toxic) and sulfuric acid.  They are capable of discharging extremely high currents if they are short-circuited (very dangerous).
* They should be located in a place where only qualified people have access to them.
* Batteries also may emit hydrogen gas which is explosive, so they require ventilation.



## 2.05 Accessible Battery Location
* Must be easy to get to all batteries for maintenance
* Possible locations include a ventilated box in a garage or a outdoor, ventilated enclosure.  May require mechanical ventilation depending on code official and location.
* They should also be close to the inverter since large currents can flow from the batteries to the inverter.



## 2.04, 2.05 Battery Location and Accessibility
* Location: Garage
* NEC 110.26 specifies several conditions, in general three feet of working depth space is required for servicing energized equipment under 150 V.

* There is enough space for just four batteries.



## 2.06 Generator Location
* Can offer some advantages in utility-interactive systems:
* PRO
* Good for long periods without sun or utility
* Keeps battery bank small

* CON
* Requires outdoor enclosure to minimize noise
* Exhaust system required
* Needs to be run monthly
* Fuel storage





## Final Recommendation


## Watts Site Survey
* 75’


## Sketch Roof Area
* Indicate Locations and Dimensions for
* The PV Array
* Balance of System Equipment (BOS) - includes wire, junction boxes, inverters, batteries
* Shading Areas
* Hazards




## Site Survey Checklist
* General Information
* Roof Assessment
* Inverter, Utility Access, Batteries, and Engine-Generator
* Recommendation
* Site Inspector Information
* Sketch of Roof Area and Proposed Array Location



## Safety Precautions
* Roof and electrical work is dangerous!
* Safety measures required:
* Sunscreen
* Fall Avoidance/Protection
* Electrical Equipment Clearances [110.26]
* Maintain Proper Grounding [250]
* Battery spill/gas/shock precautions



* Falls are the leading cause of deaths in the construction industry.
* Most fatalities occur when employees fall from open-sided floors and through floor openings.
* Falls from as little as 4 to 6 feet can cause serious lost-time accidents and sometimes death.
* Open-sided floors and platforms 6 feet or more in height must be guarded.
* Falls in Construction

Notes:
* The issues of how to provide fall protection for employees at construction sites are difficult ones.  There are so many different types of work and so many different kinds of fall hazards that it is not possible to organize fall protection into a neat set of rules that fit all situations. OSHA reflects this difficulty when it places its rules for fall protection in several different subparts in the Construction Standards, depending primarily on the nature of the work being undertaken.  There are separate locations, for example, for fall protection during work on scaffolds, during work on certain cranes and derricks, during work in tunnels, during work on stairways and ladders, during steel erection, etc.


* Personal Fall
* Arrest System
* (PFAS)
* Guardrails
* Safety Net
* Fall Protection Options

Notes:
*  Reference 1926.501(b)(1)
*
* General rule:
*
* What type of fall protection will I need?
* In most cases, a guardrail system, a safety net system, or a personal fall arrest system must be used. In some cases fences, barricades, covers, equipment guards or a controlled access zone may be used.

* Employees must be protected not just from falling off a surface, but from falling through holes and from having objects fall on them from above.


* You must be trained how to properly use PFAS.
* PFAS = anchorage, lifeline and body harness.
* Personal Fall Arrest Systems

Notes:
* Reference 1926.502(d)

* What will my personal fall arrest system do to protect me?
* A personal fall arrest system places the employee into a body harness that is fastened to a secure anchorage so that he/she cannot fall.  Body belts are not acceptable as personal fall arrest systems.  A few key requirements:
*      There should be no free fall more than 6 feet.
*      There should be prompt rescue after a fall.
*      PFAS’s must be inspected prior to each use.
*      PFAS’s must not be used until they have been inspected by a competent person.

## Electrical Clearances [110.26]


## Proper, Continuous Grounding
* NEC 250, 690.41-49

* INVERTER
* +
* -
* +
* -


## Proper, Continuous Grounding
* NEC 250, 690.41-49

* +
* -
* +
* -
* CAP + WIRE


## Battery Precautions
* Spills
* Enclosure should hold spilled electrolyte
* Have plenty of neutralizer available
* Lead-Acid: baking soda; NiCad: vinegar (acetic acid)


* Gas
* Vent battery compartment [480.9(A)]

* Shock
* Overcurrent protection, nonconductive racks
* Aprons, goggles, gloves

* Fireproofing?
* 100+ gallons of electrolyte: enclosure needs 2-hour fire barrier [2003 IBC, Section 706]




## Battery Neutralizer
* Regular lead acid systems with &gt;50 gallons of electrolyte:
* IFC 608.4 Spill Control and Neutralization
* “neutralization…method and materials shall be capable of controlling and neutralizing a spill from the largest lead-acid battery to a pH between 7.0 and 9.0.”

* VRLA systems with &gt;50 gallons of electrolyte
* IFC 609.5 Neutralization
* “neutralization…method and materials shall be capable of controlling and neutralizing a release of 3 percent of the capacity of the largest VRLA cell or block in the room to a pH between 7.0 and 9.0.”




## Tasks for a PV Installer
* Conducting a Site Survey
* Selecting a System
* Installing the System
* System Checkout & Inspection


## Review and Summary: Section 2
* Suppose the Watt’s move to Cleveland, Ohio.  How does that change the site survey?
* Look at the Site Survey Checklist included for the Watt’s house.  Disregarding the move from Florida to Ohio, which two or three factors would have the greatest impact on whether or not the PV system should be approved for the site?  Be prepared to defend your choices.
