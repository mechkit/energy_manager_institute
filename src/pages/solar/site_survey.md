# Site Survey

## Preparation

### Section 3:
Planning, Site Survey

## Learning Objectives

* After completing this chapter, you will be able to properly:
  * Identify typical tools and equipment required for conducting site surveys.
  * Establish a suitable location with proper orientation for installing a PV array.
  * Establish suitable locations for installing inverters, batteries and other balance-of-system components.
  * Use customer expectations and site characteristics to determine the best locations and layout for a PV array and other system components.


## Contents

__Background__ ❱ Introduction to Customer ❱ Why a Site Survey?

__Permits, Licensing, etc.__ ❱ Permitting ❱ Utility Agreements ❱ HOA’s ❱ Incentives

__Site Survey Checklist__ ❱ Sample Checklists ❱ Tools ❱ Roof ❱ Components

__Array Location__ ❱ Space Requirements ❱ Shading Analysis ❱ Impact on Energy

## Background

* First Steps in the Installation Process
* Introduction to Customer
  * Learn Customer’s Needs
  * Establish System Type
  * Sales Opportunity
  * Get Basic Site Information (Address, etc.)
* Project Planning

Are the customer’s goals realistic?

    Notes: The site survey is a good opportunity to understand the customers wishes. A customer’s requirements will dictate the type of system needed (e.g. a wish for power when the utility is down implies a battery back-up system –a major design change from grid tied w/o batteries). Many customers will have unrealistic expectation of their prospective PV system.  The initial visit is a good opportunity to educate the customer.

## Case Study - Introduction

* Family of four living in Central Florida
* Wish to reduce their electricity bills by 50%
* Being good global citizens, the family would like to reduce their negative impact on the planets resources
* Budget limitations likely to limit the size of the system
* Not worried about having electricity when the grid is down

## Case Study

After a discussion with the customer, you decide to perform a site survey to evaluate the home for solar  applications

* From Initial Discussion:
  * Customers Name and Address
  * Local Utility Information


## Why Perform a Site Survey?

* The site can “make or break” the project
  * Shading
  * Space Limitations
  * Accessibility…
* Site characteristics will impact how much energy can be generated!
* A well documented initial site survey will be useful throughout the project.

## Permitting

* Learn your local requirements!
  * Requirements are set by the local jurisdiction
  * Typically either the city or county
  * Requirements can vary significantly between jurisdictions
* Code Issues
  * Structural
  * Fire Safety
  * Electrical


## Solar ABCs

![Solar ABCs](PV_site_survey_SolarABCs.png)

* Solar America Board of Codes and Standards
* [solarabcs.org](http://www.solarabcs.org/)
* Expedited Permitting Process Report

## Solar ABCs: Expedited Permitting

* “…organized permitting process by which a majority of small PV systems can be permitted quickly and easily.”
* Process forms available:
  * [solarabcs.org/permitting/pdfs/Fill-in-form-doc3.pdf](http://www.solarabcs.org/permitting/pdfs/Fill-in-form-doc3.pdf)
* Recommended for installers, jurisdictions
* Limitations for high wind areas

## Licensing

* Florida PV systems can be installed by:
  * State-licensed Electrical Contractor (EC)
  * State-licensed Solar Contractor (CVC)
  * …as allowed by local AHJ


## Utility Information

* Interconnection Agreements
* Metering Requirements
* Incentives
* Customer Information (from the customer)
  * Utility Records
  * Historical Energy Use


## Utility – Online Resources

![Energy Usage History](PV_site_survey_energy_usage_history.png)

## Home Owners Associations (HOA’s)

* Generally concerned with aesthetics
* Can be more restrictive than the permitting authority
* Solar Access Laws
* [www.fsec.ucf.edu/en/consumer/solar_hot_water/q_and_a/rights.htm](http://www.fsec.ucf.edu/en/consumer/solar_hot_water/q_and_a/rights.htm)

## DSIRE
* [http://www.dsireusa.org/](http://www.dsireusa.org/)

* Federal, State , Local, and Utility Incentives
* Rules, Regulations, and Policies

## Site Survey Checklist
### Text Book

* Supplemental CD

* FSEC

* FSEC website

## Site Survey Tools

* Name and address of client (and directions)
* Camera
* Tape measure
* Ladder
* Solar Pathfinder or SunEye
* Site survey sheets
* Clipboard, pencils and ruler
* Calculator
* Inclinometer
* Compass
* Flashlight
* Sunscreen and hat

## General Information – Case Study
## Case Study – Online Mapping Tools
* Google Maps
* Bing Maps, “Bird’s-Eye View” from west
## Case Study - Initial Assessment

* Probable location

* Rooftop mounted system?
* Ground mounted?
* Space limitations?


## The Roof
### Roof Inspection

* Roofing Material
* Condition
* Orientation and Tilt
* Obstructions
* Solar Access (shading)
* Access to the Roof

### Each roof type will have unique mounting requirements!
## The Roof
## Roof Slope
## Roof Orientation

* Compass Bearing

* Remember magnetic declination!

* Online Mapping Tools Orient to True North

## The Roof Condition

* Signs of aging
* Older homes may have roofs that are ready to be replaced
* Is a roof replacement needed within a few years?

* Best to replace the roof prior to PV installation

* Consult a Roofer!

## The Roof –Supporting Structure

* The PV Array must be tied to the supporting structure
* Access to the attic may influence mounting options

## Shading Analysis

* Seek a location without shading between the hours of 9AM and 3PM, year round
* Consider future growth of vegetation

* Landscape maintenance?
* Tree removal?

* Keep in mind possible future use of neighboring properties

## Shading Analysis
## Shading Demo

* Google SketchUp

## Shading Analysis

* Even partial shading can be very detrimental to energy production
* Shading may impact system design choices

* Inverter types
* Wiring layout


## Shading Analysis
* Image Credit: Sun Light & Power
## Shading Analysis Tools
* Solar Pathfinder
## Shading Analysis Tools
* Solmetric SunEye
## Area Requirements

* The area required for a PV array will depend on the nominal array size as well as the efficiency of the modules
* In general: 80 sq ft per kW for crystaline modules (including mounting, etc.)

## Area Requirements

* Allow access for maintenance
* Remember appropriate set-backs for wind loading requirements and fire codes

## Balance of System (BOS) Components
### Evaluate Site for BOS Component Locations

* Inverter
* Batteries
* Meters
* Wire Runs
* Etc.

## Inverters

* Most direct grid-tied inverters are rated for out-door use

* Select a north wall/shaded location to keep inverter cool
* Keep away from direct living areas (inverters do make some noise)
* Place near the utility meter and load panel

* Most bi-modal inverters require a semi-conditioned location, indoors

* Place in a garage or utility room
* Place near the batteries
* Place near the utility meter and load panel


## Batteries

* Cool, covered area

* Outside in exterior-rated enclosure, north side of house (but can’t freeze)
* Or, inside garage (ventilate garage if it’s hot in the summer)
* As close as possible to inverter
* Accessible for maintenance, inaccessible to kids and stray power tools


## Case Study Review

* Array on west side
* Laying out array, up to 6kW could fit while keeping several feet away from roof edges
* Inverter on west side of house, near meter
* No batteries

## Summary

* What tools and equipment will you need when conducting site surveys?
* What regulatory steps need to be completed before installation can start?
* What are the best locations for the array and other components?


