# Fundamentals

## Learning Objectives

After completing this chapter, you will be able to properly:
* Describe Basic Solar Terminology
* Understand the Difference between Solar Irradiance and Insolation
* Diagram the Sun’s Path across the Sky Throughout the Year
* Determine Proper Array Orientation
* Understand Solar Radiation Data

## Contents

__Solar Energy__ ❱ Radiation ❱ Irradiance ❱ Insolation

__Sun Path__ ❱ Seasonal Variations ❱ Time and Location ❱ Weather

__Tilt and Azimuth__ ❱ Optimum Orientation ❱ Site Specific Issues ❱ Impact on Energy

__Solar Data__ ❱ NREL TMY3 ❱ Example ❱


## Solar Radiation

![Astronomical Unit](PV_fundamentals_AU.png)
* Even over a vast distance, an enormous amount of energy reaches Earth from the sun.
* The Earth continually receives about 170 million GW of power from the sun



## Irradiance (Power/Area)

![Solar Irradiance](PV_fundamentals_Solar_Iradiance.png)

Irradiance is a measure of the Sun’s INTENSITY

* Measured as power per unit area (W/m2)
* Varies with distance from sun
* It is an INSTANTANEOUS value

Reference: Inverse square law
http://hyperphysics.phy-astr.gsu.edu/hbase/forces/isq.html


## Solar Constant

![Solar Constant](PV_fundamentals_Solar_Constant.png)

The “Solar Constant” (Ec) is the intensity of the Sun’s radiation at the edge of our atmosphere (in space)

Ec averages 1,366 W/m2

Ec = 1,366 W/m2


## Atmosphere

The sun’s energy has to pass through obstacles before reaching PV modules at the Earth’s surface:

* Earth’s atmosphere
* Clouds, rain, humidity
* Dust and pollution
* Latitudinal “thickness” differences

## Atmosphere Effects

![Atmospheric Effects](PV_fundamentals_Atmospheric_Effects.png)
* Direct Radiation
* Diffuse Radiation
* Albedo


## Peak Sun

![Peak Sun](PV_fundamentals_Peak_Sun.png)

* Around solar noon
* Clear day
* Module or array perpendicular to the suns rays
* Irradiance = 1000 W/m2

## Irradiance

![Irradiance](PV_fundamentals_Irradiance.png)

* Obstructions impact Solar Irradiance
* Irradiance fluctuates throughout the day


## Solar Irradiance Measurement

![Solar_Meter](PV_fundamentals_Solar_Meter.png)

* Inexpensive Solar Meter
* Hand-held Unit from Daystar
  * [zianet.com/daystar/solarmeter.html](http://www.zianet.com/daystar/solarmeter.html)



## Solar Insolation (Energy/Area)

* Solar Insolation is a measurement of irradiance over time
* Represents an amount of energy
* Measured in kWh/m2 or Peak Sun-Hours
* Also called Irradiation


Remember:

    Power   →  W, or kW
    Energy  →  Wh, or kWh


## Insolation

![Insolation](PV_fundamentals_Insolation.png)

![Insolation 2](PV_fundamentals_Insolation_2.png)

## Solar Insolation (Peak Sun Hours)

![Peak sun hours 1](PV_fundamentals_Peak_sun_hours_1.png)

![Peak sun hours 2](PV_fundamentals_Peak_sun_hours_2.png)

![Peak sun hours 3](PV_fundamentals_Peak_sun_hours_3.png)

![Peak sun hours 4](PV_fundamentals_Peak_sun_hours_4.png)

## Solar Insolation and PV

Solar insolation data is used to calculate the energy output of an array.

Example: PV array produces 3 kW AC at peak sun.  How many kWh will it produce if it receives 5 sun-hours?

    3 kW AC x 5 Sun-hours = 15 kWh AC


## Solar Insolation and PV

![Solar Insolation and PV](PV_fundamentals_Solar_Insolation_and_PV.png)

## Sun Path

Solar radiation varies with:
* Time of day (Earth’s daily rotation)
* Seasons (Earths annual rotation around the sun)
* Latitude (Location on Earth’s surface)
* Climatic Conditions (weather, etc.)
* Elevation

## Atmosphere and Time of Day

![Atmosphere and Time of Day](PV_fundamentals_Atmosphere_and_Time_of_Day.png)

## Seasons

![Seasons](PV_fundamentals_Seasons.png)


## Seasons (and Latitude)
Summer (northern hemisphere)
Winter (northern hemisphere)

![Seasons and Latitude](PV_fundamentals_Seasons_and_Latitude.png)

## Sun Path - Seasons

![Sun Path - Seasons](PV_fundamentals_Sun_Path-Seasons.png)

![Sun Path - Seasons](PV_fundamentals_Sun_Path-Seasons_2.png)

## Tilt and Azimuth

![Tilt and Azimuth](PV_fundamentals_tilt_and_azimuth.png)


## Tilt Angle

![Tilt angle](PV_fundamentals_tilt_angle.png)


## Azimuth

Maximum energy production with array facing South (North in the southern hemisphere)
* Determine direction using a compass
* Note the difference between true south and magnetic south
  * At FSEC, true south is five degrees west of magnetic south – check thru NOAA web site
    http://www.ngdc.noaa.gov/geomag/declination.shtml


## Magnetic Declination

http://www.ngdc.noaa.gov/geomag/declination.shtml

## Magnetic Declination

![Magnetic Declination](PV_fundamentals_magnetic_declination.png)

![Magnetic Declination](PV_fundamentals_magnetic_declination_2.png)

## Array Orientation

How critical is it?

![Array Orientation](PV_fundamentals_array_orientation.png)

## Roof Orientation

Residential PV arrays are generally mounted in the same plane as the roof

* Aesthetics
* Wind Loading
* Minimal losses from “optimum” orientation

![Roof Orientation](PV_fundamentals_roof_orientation.png)

## Solar Radiation Data

* National Renewable Energy Laboratory (NREL)
* Solar radiation data for many U.S. locations
* TMY = Typical Meteorological Year

![Solar Radiation Data](PV_fundamentals_solar_radiation_data.png)


Link: Pvwatts.nrel.gov

## Solar Radiation Data

* Various tilt angles
* South facing and tracking
* Climate Conditions
* Textbook resource example (p. 49)

![Solar Radiation Data](PV_fundamentals_solar_radiation_data_2.png)

Link: [NREL data](https://maps.nrel.gov/nsrdb-viewer/#/?aL=8VWYIh%255Bv%255D%3Dt&bL=groad&cE=0&lR=0&mC=39.16414104768742%2C-88.8134765625&zL=4)

## Summary

* How does incident sunlight vary during a day and between seasons?
* What is the difference between solar Irradiance and Insolation?
* A home sees fog roll in at around 2pm between spring and fall each year. What’s the best orientation for this array?
* What is a sun-hour?
* A roof faces at a heading of 165°. How can the contractor make the roof-mounted array face true south?

Note: Heading of 165… face the array at 165! No need to make it match the 180 heading exactly.


## Solar System Configurations

### Bimodal

![bimodal systems](solar_system_cofiguration_bimdal.jpg)

* Battery Backup
* Uninterrupted power


### Interactive

![Utility-Interactive Systems](solar_system_cofiguration_interactive.jpg)

* Less Maintenance
* Lowest Cost

Notes:
* Common Types of PV System applications
* Key features and benefits of each kind of system
* Key disadvantages of each kind of system
* 102-110

## Summary
* Expectations from this course
* Solar driven by incentives/policies over next few years
* Price of electricity is increasing;price of solar is decreasing
