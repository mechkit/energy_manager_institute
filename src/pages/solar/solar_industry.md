# Solar Industry

## Value Chain

Manufacturers **➜** Distributors **➜** Installers **➜** Home Owners, Commercial and Industrial, Utilities

## Organizations

Solar Energy Industries Association Local Chapters

![Solar Energy Industries Association Local Chapters](PV_industry_organizations.png)

[SEIA.org](SEIA.org)

Note: Roles of Segments of PV industry:
* Mw installed and manufacturing figures by country
* SEIA and local chapters, plus MREA, SEBANE, etc.
* Manufacturers -> distributors -> installers
*
Market indicators, value propositions, and opportunities

## Market Indicators

![Price of electricity](PV_industry_indicators_10.png)

source: [Energy Information Administration](http://www.eia.gov/electricity/data/browser/#/topic/7?agg=2,0,1&geo=g&freq=M)

![Market Indicators](PV_industry_indicators_1.png)

source: [Data and Statistics - IRENA REsource](http://resourceirena.irena.org/gateway/dashboard/?topic=4&subTopic=18)

- Market indicators, value propositions, and opportunities
- Market demand is driven by incentives and the price of electricity.
- The price of electricity is going up
- Incentives. The best incentive seems to be the feed-in-tariff

source: [The Shifting Relationship Between Solar and Silicon in Charts | Greentech Media](https://www.greentechmedia.com/articles/read/Solar-and-Silicons-Shifting-Relationship-in-Charts)

![Market Indicators](PV_industry_indicators_2.png)

source: [Solar photovoltaic](https://www.irena.org/costs/Charts/Solar-photovoltaic)

![Market Indicators](PV_industry_indicators_3.png)
![Market Indicators](PV_industry_indicators_4.png)
![Market Indicators](PV_industry_indicators_5.png)
![Market Indicators](PV_industry_indicators_6.png)
![Market Indicators](PV_industry_indicators_7.png)

source: [Data and Statistics - IRENA REsource](http://resourceirena.irena.org/gateway/dashboard/?topic=3&subTopic=32)

![Market Indicators](PV_industry_indicators_8.png)

From SEIA Market Insight

![Market Indicators](PV_industry_indicators_9.png)

http://www.seia.org/research-resources/solar-market-insight-report-2013-q2

## Resource

![Resource](PV_industry_resource.png)

## Est. Payback

[State by State Solar Energy ROI (2010)](http://solarpowerrocks.com/reports/spr-report-card-2010-part-1-state-by-state-solar-energy-roi/)


## Why We’re Here

![Expected Global Energy Usage](PV_industry_Why_Were_Here.png)

Source: EIA, International Energy Outlook 2010

Note: Over the next 2.5 decades we’ll be increasing global energy usage by 50%! Gotta make that energy somehow…
Organization for Economic Co-operation and Development


## Working in the solar industry

## North American Board of Certified Energy Practitioners (NABCEP)

The [NABCEP](https://www.nabcep.org/)
is a nonprofit professional certification and accreditation organization that offers both individual and company accreditation programs for photovoltaic system installers,
solar heat installers, technical sales, and other renewable energy professionals throughout North America.
NABCEP was officially incorporated in 2002 and its mission is to raise standards while promoting consumer and other stakeholders' confidence within the renewable energy industry.
(source [Wikipedia](https://en.wikipedia.org/wiki/North_American_Board_of_Certified_Energy_Practitioners))

