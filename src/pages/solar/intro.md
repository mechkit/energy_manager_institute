# Solar Guide

![PV pannel|class:image_full_width](PV.png)

## Watt do you want to teach?

- [Economics](economics.md)
- [Fundamentals](fundamentals.md)
- [Large Scale](large_scale.md)
- [Lifespan](lifespan.md)
- [NEC](NEC.md)
- [Overview](overview.md)
- [PV](PV.md)
- [Photovoltaic Modules](PV)
- [Planning](planning.md)
- [Power Consumption Estimation](power)
- [Site Survey](site_survey.md)
- [Sizing](sizing.md)
- [Solar Fundamentals](solar_fundamentals.md)
- [Solar Industry](solar_industry.md)
- [System Calculations](system_calculations.md)
- [System Design](system_design)

