# Overview
Welcome to the FSEC installer training. In this first section I’ll give you an introduction to the course

## Learning Objectives
* Logistics
* Expectations from this course
* Course organization
* Structure of each section
* Recognize different system configurations
* Understand solar industry

## Contents Slide in Each Chapter

__Course__ ❱ Logistics ❱ Objectives ❱ Expectations

__Sections__ ❱ Section Outlines ❱ Section Format

__System Configurations__

__Solar Industry__ ❱ Value Chain ❱ Organizations ❱ Market

## Course Objectives

### Tasks
* Sales
* Installation
* Managing Project
* Maintenance

### Types
* Self Regulating
* Stand Alone
* Utility Interactive
* Bimodal
* Hybrid

### Focus
* Residential
* Small Commercial
* Large Commercial
* Power Plants

## Goal

By the end of this course, you should be able to correct installation and maintenance procedures include:
* Conduct a site assessment
* Select the proper system design for an application
* Adapt the mechanical and electrical designs???
* Know how to perform a system checkout and inspection
* Understand system maintenance and troubleshooting

## What to Expect

### This Course
* Basics of PV
* Common Installations
* Demonstrate Basic Knowledge

### Qualification
* This course does not qualify you to install solar PV systems
* The Authority Having Jurisdiction still qualifies you to install systems
* In the State of Florida must be a licensed
* NABCEP Installer Certification for more experienced installers
* Must have solar contractor’s license or electrical contractor’s license

### NABCEP
* Handout for various certifications (entry level, PV installers, new thing, UL training)
* Demonstrate basic knowledge
* What you can expect from this course

### Contractor Licensing (Handout) (DBPR Internet Resource)
* Scope of CV vs. EC ???
* Prequalifications Notes on other states
* Discussions about FSEC certificate vs. certification

## Course Outline

### Introduction
* Section 1: Overview
* Section 2: Fundamentals

### Preparation
* Section 3: Site Survey
* Section 4: System Selection
* Section 5: PV Array

### Installation
* Section 6: Mounting
* Section 7: Wiring
* Section 8: Safety

### Other Components
* Section 9: Inverter
* Section 10: Batteries

### Finishing
* Section 11: Utility Interconnection
* Section 12: Acceptance Test

