# Solar Guide Overview

![PV pannel|class:image_full_width](PV.png)

Watt do you want to know?

## Fundamentals

- [Solar Fundamentals](solar_fundamentals.md)
- [Fundamentals](fundamentals.md)
- [Photovoltaic Modules](PV.md)
- PV System Components
- PV Technology
- Systems Configuration
- [Large Scale](large_scale.md)

## Solar Industry

- [Solar Industry](solar_industry.md)

## Safety

- [Safety](safety.md)

## Preparation

- Site Survey
- Solar Resource Assessment.
- System Selection
- PV Array

## Design

- [System Design](system_design.md)
- [System Calculations](system_calculations.md)
- [Economics](economics.md)
- [Lifespan](lifespan.md)
- [NEC](NEC.md)
- [Planning](planning.md)
- [Power Consumption Estimation](power)
- [Site Survey](site_survey.md)
- [Sizing](sizing.md)

## Installation

- Mounting
- Wiring
- Safety

## Other Components

- Inverter
- Batteries

## Commissioning

- Utility Interconnection
- Acceptance Test

## Support

- System diagnostics and troubleshooting
- Maintenance
- Upgrades



