# System Design

## Basic System Design

![Solar Cell](simple_PV.png)

## Solar System Configurations

- Grid-tie (Utility Interactive)
  - Less Maintenance
  - No battery backup
  - Lowest Cost
  - Size for space and budget
- Stand-Alone
  - No grid connection
  - Batteries store power
  - Size for total supply for loads
  - Rarely will not have an inverter
- Multi-mode
  - Battery backup and grid connection
  - More than one type of generation
  - Size for Backup of critical Loads
  - Uninterrupted power

# Inverter

The inverter converts the direct current (DC) output of a photovoltaic (PV) solar panel into a alternating current (AC) that is compatible with the utility grid.

The inverter usually provides maximum power point tracking for the PV array. It adjusts the load to maximize the power for the current conditions.

The inverter also manages the AC output to match the utility grid, including disconnecting when needed (anti-islanding protection).

## Inverter types

### String

A traditional PV system, with one inverter per array, fed by multiple strings of modules.

### Micro

Instead of one central inverter, each module has a dedicated inverter.

### Optimizer


## Cost

Often the key factor when choosing the size of a photovoltaic system is the cost. Costs are...

## Size

### Energy and Power

Kilowatt Hours (Energy):
- Amount of electricity consumed or produced over a period of time
- Think: Odometer
- Example: Output of 100 kWh in a month

Kilowatts (Power):
- Amount of electricity being consumed or produced at an instant
- Think: Speedometer
- Example: System Capacity of 4kW




## Array placement

### Tilt and Azimuth

![Tilt and Azimuth](PV_fundamentals_tilt_and_azimuth.png)


### Tilt Angle

![Tilt angle](PV_fundamentals_tilt_angle.png)


### Azimuth

Maximum energy production with array facing South (North in the southern hemisphere)
- Determine direction using a compass
- Note the difference between true south and magnetic south
  - At FSEC, true south is five degrees west of magnetic south - check thru NOAA web site
    http://www.ngdc.noaa.gov/geomag/declination.shtml


### Magnetic Declination

[NOAA: Magnetic Declination](http://www.ngdc.noaa.gov/geomag/declination.shtml)

![Magnetic Declination](PV_fundamentals_magnetic_declination.png)

![Magnetic Declination](PV_fundamentals_magnetic_declination_2.png)

### Array Orientation

How critical is it?

![Array Orientation](PV_fundamentals_array_orientation.png)

### Roof Orientation

Residential PV arrays are generally mounted in the same plane as the roof

- Aesthetics
- Wind Loading (Roof Zones)
- Minimal losses from “optimum” orientation

![Roof Orientation](PV_fundamentals_roof_orientation.png)

### Shading



## Electrical Fundamentals
### Series and Parallel Wiring






