## Energy and meteorological simulation

It is used to fill in any gaps in meteorological data, or to completely simulate all data when there is not instrumentation. This is needed during development, but also can be used in schools that do not yet have the needed PV and/or weather instrumentation. It can also be used to run "what if" scenarios.

A proof-of-concept server can produce vaguely realistic data given a date and time. It takes into account weather averages for central Florida, and attempts to produce data that mimics real weather conditions. The data is random, but procedural. It will always calculate the same results for the same date and time.

See [data server](./data_server) for a data sample.

The code currently resides as a sub-module of the energy_data_server.

Repository: [Energy Data Server](https://gitlab.com/mechkit/energy_data_server). Specific sub-module: [function](https://gitlab.com/mechkit/energy_data_server/blob/master/mk_weather.js).
