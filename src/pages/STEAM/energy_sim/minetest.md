## Minetest

A modification for the open source "game" [Minetest](https://www.minetest.net/).

This is still in a proof of concept stage. It takes data from the [data server](./data_server), displays the data in game, and sets the daytime based on the server data.

[![Minetest Sample|width:900,class:image_center](Minetest_sample_1.png)](Minetest_sample_1.png)

Repository: [Mintest Energy Manager](https://gitlab.com/mechkit/minetest_energy_manager)
