## Data use

* The [Data server](./data_server) includes a simple live data display. This can be used as a system monitor, or as inspiration for a more complex display.
  * Desktop and mobile versions:
[![Energy Manager Sample Full|width:500](energy_manager_full.png)](energy_manager_full.png)
[![Energy Manager Sample mobile|width:500](energy_manager_mobile.png)](energy_manager_mobile.png)

* Modules (AKA libraries), for Node and Python would make it easier for students to build projects that consume data from the data server.
* A language like [Scratch](https://scratch.mit.edu/) will be selected for introductory programming classes. (Need teacher input for selection.)
* Development of [Minetest](./minetest) mods would provide a lot of learning opportunities.
  * Modeling the school in the Minetest world: Familiarity and connection to school; spacial awareness.
  * Monitoring of PV output: Learning the connection between weather conditions and energy output. This is also key to keeping the real system running well.
  * Monitoring school energy use and production: Teaches a practical understanding of core STEM principles. It also might encourage the students and school to make better energy management decisions.

The current focus is on the data's use in [minetest](./minetest).
