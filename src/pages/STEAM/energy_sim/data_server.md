## Data server

Provides data in a consistent format (API). It will combine measured and simulated sources. It serves as a hub for the other sections of the this project.

A proof-of-concept server provides data in the JSON format. During initial testing it randomly selects a date and time, and requests meteorological and system data from the [simulation](./simulation).

Sample data:

  date         	"19731012"
  time         	"141201"
  hours        	"14.20"
  day_fraction 	"0.59"
  sun_heigh    	"0.94"
  rain_today   	"true"
  rain_chance  	"32.00"
  rainy_hour   	"false"
  cloudy_hour  	"true"
  cloud_cover  	"0.07"
  heat_heigh   	"0.97"
  high         	"84.36"
  low          	"69.75"
  air_temp     	"84"
  irr_ideal    	"943.07"
  irr          	"943.00"
  request_name 	"mk_weather"
  request_time 	"1"


Repository: [Energy Data Server](https://gitlab.com/mechkit/energy_data_server)
