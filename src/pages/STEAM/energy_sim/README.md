---
sidebar: auto
sidebarDepth: 2
---

## Planned topics and tools

| Section                               | Desc.                                                        | Lesson(s)                                     |
|:--------------------------------------|:-------------------------------------------------------------|:----------------------------------------------|
| [Data collection](./data_collection) | Server: collects PV and energy instrumentation data          | instrumentation, programming                  |
| [Simulation](./simulation)           | Server: Simulates missing or predicted data                  | math, physics, engineering                    |
| [Data server](./data_server)         | Server: provides data in consistent format.                  | management, programming, network/server admin. |
| [Data use](./output)                 | Multiple sub-project that will analysis and display the data | practical engineering, programming            |

## References

* [SunSmart E-Shelter Program](http://www.fsec.ucf.edu/en/education/sunsmart/index.html)
* [EnergyWhiz](http://www.energywhiz.com/)

## Development

* [The plan, development status](https://gitlab.com/mechkit/energy_manager_institute/boards)
