# Solar Cooking

##
In this two-part cook-off, students design and build solar thermal “ovens” to cook their tasty creations.
Then, students present their solar-cooked dish to a panel of judges. Students compete for best solar cooker design, as well as a culinary award.

Students show off their knowledge and abilities in engineering and construction. Likewise, they also show their problem-solving skills.
