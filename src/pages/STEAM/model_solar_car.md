# Model Solar Car

##
In this event, each team produces a student-built, model size solar electric car.
Each car is judged on technology, craftsmanship, innovation and appearance.
Then, cars race on a 20-meter track in a head-to-head race.
This event challenges students to use their science, creative thinking, and teamwork skills.
