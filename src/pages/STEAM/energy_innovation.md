# Energy Innovation

##
This activity requires students to work together to design and market a full-scale photovoltaic (solar electric) powered device that has real-world use.
Design awards are judged based on creativity, construction, message and marketing of the product.
