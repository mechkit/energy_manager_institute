# Energy Simulation



## Energy production and use data collection.

Where real world data can be collected, instructions and/or software must be created to connect each instrumentation data source to the data server.

This section of the project may have less educational merit, but it is critical for the other parts to work. It is also key to allowing the students to help the school keep their energy projects working, both for the small financial gain and for the emergency shelter operation. The lesson may be: "Sometimes you have to do the boring stuff, if you want to do the fun stuff".


## Energy and meteorological simulation

It is used to fill in any gaps in meteorological data, or to completely simulate all data when there is not instrumentation. This is needed during development, but also can be used in schools that do not yet have the needed PV and/or weather instrumentation. It can also be used to run "what if" scenarios.

A proof-of-concept server can produce vaguely realistic data given a date and time. It takes into account weather averages for central Florida, and attempts to produce data that mimics real weather conditions. The data is random, but procedural. It will always calculate the same results for the same date and time.

See the data sample below.

The code currently resides as a sub-module of the energy_data_server.

Repository: [Energy Data Server](https://gitlab.com/mechkit/energy_data_server). Specific sub-module: [function](https://gitlab.com/mechkit/energy_data_server/blob/master/mk_weather.js).

## Data server

Provides data in a consistent format (API). It will combine measured and simulated sources. It serves as a hub for the other sections of the this project.

A proof-of-concept server provides data in the JSON format. During initial testing it randomly selects a date and time, and requests meteorological and system data from the simulation.

Sample data:

  date         	"19731012"
  time         	"141201"
  hours        	"14.20"
  day_fraction 	"0.59"
  sun_heigh    	"0.94"
  rain_today   	"true"
  rain_chance  	"32.00"
  rainy_hour   	"false"
  cloudy_hour  	"true"
  cloud_cover  	"0.07"
  heat_heigh   	"0.97"
  high         	"84.36"
  low          	"69.75"
  air_temp     	"84"
  irr_ideal    	"943.07"
  irr          	"943.00"
  request_name 	"mk_weather"
  request_time 	"1"


Repository: [Energy Data Server](https://gitlab.com/mechkit/energy_data_server)



## Data use

* The Data server includes a simple live data display. This can be used as a system monitor, or as inspiration for a more complex display.
  * Desktop and mobile versions:
![Energy Manager Sample Full](energy_manager_full.png)
![Energy Manager Sample mobile](energy_manager_mobile.png)

* Modules (AKA libraries), for Node and Python would make it easier for students to build projects that consume data from the data server.
* A language like [Scratch](https://scratch.mit.edu/) will be selected for introductory programming classes. (Need teacher input for selection.)
* Development of Minetest mods would provide a lot of learning opportunities.
  * Modeling the school in the Minetest world: Familiarity and connection to school; spacial awareness.
  * Monitoring of PV output: Learning the connection between weather conditions and energy output. This is also key to keeping the real system running well.
  * Monitoring school energy use and production: Teaches a practical understanding of core STEM principles. It also might encourage the students and school to make better energy management decisions.

The current focus is on the data's use in Minetest.


## Minetest

A modification for the open source "game" [Minetest](https://www.minetest.net/).

This is still in a proof of concept stage. It takes data from the [data server](./data_server), displays the data in game, and sets the daytime based on the server data.

![Minetest Sample](Minetest_sample_1.png)

Repository: [Mintest Energy Manager](https://gitlab.com/mechkit/minetest_energy_manager)
