# Power consumption estimation

## Utility Information

By collecting and reviewing past energy bills, either paper or online, you can build a outline of your monthly energy use.

Example online energy use plot:

![Utility Energy Plot](utility_energy_plot.png)

Example energy use on bill:

![Utility Energy Plot](utility_energy_paper.png)


## Appliance Energy Use Estimation

Most home appliances include a energy guide that estimates the yearly energy use.

[![EnergyGuide Label](energyguide_label.jpg)](https://www.consumer.ftc.gov/articles/0072-shopping-home-appliances-use-energyguide-label)

For a more accurate measurement of you usage, appliance level energy monitoring is available.

![Image provided by: https://www.flickr.com/photos/yourbestdigs/28904164381|style.width:400px](plug_energy_meter.jpg)

## Whole House Energy Monitoring

These energy monitors can assist with calculating monthly and yearly energy totals, as well as monitoring minute-to-minute changes to your energy use.
They vary in cost and complexity/features. Some use a dedicated display, and some can be connected to a home network to share data to a computer or smartphone.
Often installed directly in the main breaker panel of the home, and therefor may require an electrician to install.

