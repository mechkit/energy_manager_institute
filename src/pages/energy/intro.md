# Introduction to Energy Management

![Energy Managment](energy_examples_subject.svg)

## Watt do you want to teach?
- [Power consumption estimation](./energy_use.md)
- [Energy Simulation](energy_simulation.md)



