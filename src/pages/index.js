import { div, span, p, a, img, ul, li, br, h1, h2, h3, input, button, select, option, table, tr, th, td, col, em, b, mark, strong, cite, i, u, s, q, object, svg, animate, animateMotion, animateTransform, circle, clipPath, ellipse, defs, desc, discard, g, hatch, hatchpath, image, line, linearGradient, marker, mask, metadata, mpath, path, pattern, polygon, polyline, radialGradient, rect, script, set, solidcolor, style, symbol, text, textPath, title, tspan, use, view } from 'specdom_helper';

export default function(){
  var specs = div([
    svg({
      width: '100%',
      height: 'auto',
      version: '1.1',
      viewBox: '0 0 209.78 170.76',
      xmlns: 'http://www.w3.org/2000/svg',
      'xmlns:cc': 'http://creativecommons.org/ns#',
      'xmlns:dc': 'http://purl.org/dc/elements/1.1/',
      'xmlns:rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
      'xmlns:xlink': 'http://www.w3.org/1999/xlink',
    }),
    img('/images/em_logo.png','EMI Logo'),
    object({
      type:"image/svg+xml",
      data:"images/campus_map.svg",
    }),
  ]);



  return specs;
}
