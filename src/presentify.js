#!/usr/bin/env node

var f = require('functions');
var fs = require('fs');
var path = require('path');
var probe = require('probe-image-size');
var specdom_tools = require('specdom_tools');
var specs_to_html = specdom_tools.specs_to_html;
var fix_link_path = specdom_tools.fix_link_path;
var markdown_specs = specdom_tools.markdown_specs;

function presentify_specdom(spec, public_path){
  if ( spec.children !== undefined && spec.children.constructor === Array ){
    let children = [];
    spec.children.forEach( subspec => {
      if ( subspec.constructor === Object ){
        let subspec_presentified = presentify_specdom(subspec, public_path);
        if ( subspec_presentified.constructor === Array ){
          subspec_presentified = subspec_presentified.map( subsubspec => {
            return presentify_specdom(subsubspec, public_path);
          });
          children = children.concat( subspec_presentified );
        } else {
          children.push( subspec_presentified );
        }
      } else {
        children.push( subspec );
      }
    });
    spec.children = children;
  }
  if ( spec.tag === 'div' && spec.props && spec.props.class === 'level_0' ){
    spec.props.class = 'reveal';
  }
  if ( spec.tag === 'div' && spec.props && spec.props.class === 'section level_1' ){
    spec.props.class = 'slides';
  }
  if ( spec.tag === 'div' && spec.props && spec.props.class === 'section level_2' ){
    spec.props.class = 'section';
    spec.tag = 'section';
  }
  if ( spec.class === 'section' || (spec.props && spec.props.class === 'section') ){
    spec = split_section(spec);
  }
  if ( spec.tag === 'img' ){
    spec.meta = spec.meta || {};
    spec.meta.src_file = path.join( __dirname, '../', public_path, spec.props.src );
    spec.meta.orientation = image_orientation(spec.meta.src_file);
  }
  if ( spec.tag === 'p' && spec.children && spec.children.constructor === Array ){
    let all_images = spec.children.every( child => {
      // if child is an empty string, do not not count it against 'p' being all images
      if ( child.constructor === String && child.trim() === '' ){
        return true;
      }
      return child.tag && child.tag === 'img';
    });if ( all_images ){ // if a 'p' only filled with images, replace the 'p' element with all the images.
      spec = spec.children.filter( child => {
        if ( child.constructor !== String ){
          return true;
        } else if ( child.trim() !== '' ){
          return true;
        } else {
          return false;
        }
      });
    }
  }
  return spec;
}

function image_orientation(src_file){
  console.log('X',src_file);
  let ext = src_file.split('.').slice(-1)[0].toLowerCase();
  //if( ['jpg', 'jpeg', 'gif', 'png', 'webp', 'bmp', 'tiff', 'svg', 'psd'].includes(ext) ){
  if( ['jpg', 'jpeg', 'gif', 'png', 'webp', 'bmp', 'tiff',  'psd'].includes(ext) ){
    var image = require('fs').readFileSync(src_file);
    let img_props = probe.sync(image);
    let orientation = ( img_props.height * 1.5 ) > img_props.width ? 'portrait' : 'landscape';
    return orientation;
  } else {
    return false;
  }
}


function split_section(spec){
  let slide_content = spec.children;
  let slide_title = false;
  if ( slide_content[0].tag === 'h2' ){
    slide_title = slide_content[0];
    slide_content = slide_content.slice(1);
  }
  /*
  slide_content.forEach( subspec => {
    if ( subspec.constructor === Object ){
      if ( subspec.tag === 'p' ){
        subspec.children.forEach( subsubspec => {
          if ( subsubspec.tag === 'img' ){
            image_count++;
          }
        });
      } else if ( subspec.tag === 'img' ){
        image_count++;
      }
    }
  });
  if ( image_count > 1 ){
  */
  let subsections = [];
  let subsection = {
    image_children: [],
    text_children: [],
  };
  slide_content.forEach( subspec => {
    if ( subspec.constructor === Object &&  subspec.tag === 'img' ){
      if ( subsection.image_children.length || subsection.text_children.length ){
        subsections.push(subsection);
        subsection = {
          image_children: [],
          text_children: [],
        };
      }
      subsection.image_children.push(subspec);
    } else {
      subsection.text_children.push(subspec);
    }
  });
  if ( subsection !== undefined && ( subsection.image_children.length || subsection.text_children.length ) ){
    subsections.push(subsection);
  }
  let subsections_children = [];
  subsections.forEach( subsection => {
    if ( subsection.text_children.length && subsection.image_children.length ){
      let table_children;
      if ( subsection.image_children[0].meta.orientation === 'landscape' ){
        table_children = [
          {
            tag: 'tr',
            children: [
              {
                tag: 'td',
                children: subsection.image_children,
              },
            ]
          },
          {
            tag: 'tr',
            children: [
              {
                tag: 'td',
                children: subsection.text_children,
              },
            ]
          }
        ];
      } else {
        table_children = [
          {
            tag: 'tr',
            children: [
              {
                tag: 'td',
                children: subsection.image_children,
              },
              {
                tag: 'td',
                children: subsection.text_children,
              },
            ]
          }
        ];
      }
      subsections_children.push({
        tag: 'section',
        children: [
          slide_title,
          {
            tag: 'table',
            props: {class: 'layout'},
            children: table_children
          }
        ]
      });
    } else {
      let subsection_children = subsection.image_children.length ? subsection.image_children : subsection.text_children;
      subsections_children.push({
        tag: 'section',
        children: [slide_title].concat(subsection_children)
      });
    }
  });
  if ( subsections_children.length === 1 ){
    subsections_children = subsections_children[0].children;
  }
  spec.children = subsections_children;
  //}
  return spec;
}



var convertion_functions = {
  'md': markdown_specs,
};
function presentify(static_path, input_path, output_path, public_path){
  output_path = output_path || input_path;
  let extentions = Object.keys(convertion_functions);
  let files = fs.readdirSync(input_path);
  files.forEach( file_name => {
    let file_name_parts = file_name.split('.');
    let ext = file_name_parts.slice(-1)[0];
    let input_file_path = path.join(input_path, file_name);
    if ( fs.lstatSync(input_file_path).isDirectory() ){
      console.log('opening: '+ input_file_path );
      let new_sub_output_path = path.join(output_path,file_name);
      if ( ! fs.existsSync(new_sub_output_path) ){
        fs.mkdirSync(new_sub_output_path);
      }
      presentify(static_path, input_file_path, new_sub_output_path, public_path);
    } else if ( extentions.includes(ext) ){
      // get markdown, convert to specdom
      console.log('converting: '+ input_file_path );
      let convertion_function = convertion_functions[ext];
      let input_string = fs.readFileSync(input_file_path, {encoding: 'utf8'});
      let page_spec = convertion_function(input_string);
      // page meta data
      let file_name_base = file_name_parts.slice(0,-1).join('.');
      let page_name = input_file_path.split(/\/|\./).slice(-2)[0];
      let location = input_file_path.split(/\/|\./).slice(2,-2);
      let title = f.pretty_name(page_name);
      let page_id;
      if ( location.length > 0 ){
        page_id = location.join('/') + '/' + page_name;
      } else {
        page_id = page_name;
      }
      page_spec.props = page_spec.props || {};
      page_spec.props['id'] = 'page';
      page_spec.meta = page_spec.meta || {};
      page_spec.meta['file_name_base'] = file_name_base;
      page_spec.meta['location'] = location;
      page_spec.meta['page_id'] = page_id;
      page_spec.meta['title'] = title;
      page_spec = fix_link_path(page_spec,{
        path: './',
        root_path: './',
      });
      page_spec = presentify_specdom(page_spec, public_path);
      if ( page_spec.children[0].children && page_spec.children[0].children[0].tag === 'h1' ){
        page_spec.children[0].children = page_spec.children[0].children.slice(1);
      }
      let reveal_config = {
        progress: true,
        slideNumber: true,
        hash: true,
        center: false,
        width: 1800,
        height: 1000,
      };
      let reveal_config_string = JSON.stringify(reveal_config);
      // convert to html and write
      let html_string = specs_to_html(page_spec,{
        title,
        css_files: [
          './reveal.css',
          './theme.css',
        ],
        js_files_post: [
          './reveal_min.js',
        ],
        final_lines: [
          `<script> console.log('SPECS:',${JSON.stringify(page_spec)}); </script>`,
          `<script> Reveal.initialize(${reveal_config_string}); </script>`,
        ]
      });
      let output_html_path = path.join(output_path, file_name_base);
      let output_html_file_path = path.join(output_html_path, 'index.html');
      console.log('writing: '+ output_html_file_path );
      if ( ! fs.existsSync(output_html_path) ){
        fs.mkdirSync(output_html_path);
      }
      fs.writeFileSync(output_html_file_path, html_string);
      // copy static support files
      [
        'reveal_min.js',
        'reveal.css',
        'theme.css',
      ].forEach( static_file_name => {
        //console.log(static_file_name);
        fs.copyFileSync( path.join(static_path, static_file_name), path.join(output_html_path, static_file_name));
      });
    }
  });
}

module.exports = presentify;
