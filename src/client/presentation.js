/**
* this is the app
* @file_overview this the starting point for the application.
* @author keith showalter {@link https://github.com/kshowalter}
* @version 0.1.0
*/
import 'normalize.css';
import dynatic from 'dynatic';
import Reveal from 'reveal.js';

import pages from './pages.json';

function presentify(spec){
  if ( spec._ === 'div' && spec._p && spec._p.class === 'level_0' ){
    spec._p.class = 'reveal';
  }
  if ( spec._ === 'div' && spec._p && spec._p.class === 'section level_1' ){
    spec._p.class = 'slides';
  }
  if ( spec._ === 'div' && spec._p && spec._p.class === 'section level_2' ){
    spec._ = 'section';
  }
  if ( spec._c !== undefined && spec._c.constructor === Array ){
    spec._c.map( subspect => presentify(subspect) );
  }
  return spec;
}

Object.keys(pages).forEach( page_name => {
  let page_spec = pages[page_name];
  pages[page_name] = presentify(page_spec);
});

var settings = {
  title: ['do not use'],
  default_page_name: 'default',
  selected_page_id:  '',
  onload: function(){
    Reveal.initialize({
      hash: true,
    });
  },
};
dynatic(pages, settings);
