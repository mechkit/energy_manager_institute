/**
* this is the app
* @file_overview this the starting point for the application.
* @author keith showalter {@link https://github.com/kshowalter}
* @version 0.1.0
*/
import 'normalize.css';
import dynatic from 'dynatic';
const expand = require('specdom_tools').expand;

import pages from './pages.json';
for ( let page_id in pages ){
  pages[page_id] = expand( pages[page_id] );
}

var settings = {
  title: ['do not use'],
  default_page_name: 'default',
  selected_page_id:  '',
};
dynatic(pages, settings);
