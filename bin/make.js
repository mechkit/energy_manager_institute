#!/usr/bin/env node
const fs = require('fs');
const fse = require('fs-extra')
const path = require('path');
const program = require('commander');
const markdown_to_specdom = require('specdom_tools').markdown_to_specdom;
const compact = require('specdom_tools').compact;
const fix_link_path = require('specdom_tools').fix_link_path;
const presentify = require('../src/presentify');
const webpack = require('webpack');
const webpack_config = require('../webpack.config.js');
const compiler = webpack(webpack_config);

var config = {};
try {
  config = require('../build_config.json');
} catch (error){
  console.log('No config file found...');
  console.log('...using defalt config.');
}
config = Object.assign({
  markdown_input_path: 'src/pages',
  presentation_static_path: 'src/presentation',
  presentation_output_path: 'src/public/presentation',
  json_output_path: 'src/client',
  input_path_relative: '',
  output_path_relative: '',
  public_path: 'src/pages',
},config);

console.log('---');
//"mk_specs": "npx dynatic mk_specs src/pages src/client;
console.log('getting pages');
function load_files(input_path, markdown_strings){
  let extentions = ['md','markdown'];
  let files = fs.readdirSync(input_path);
  markdown_strings = markdown_strings || {};
  files.forEach( file_name => {
    let file_name_parts = file_name.split('.');
    let ext = file_name_parts.slice(-1)[0];
    let input_file_path = path.join(input_path, file_name);
    if ( fs.lstatSync(input_file_path).isDirectory() ){
      console.log('opening: '+ input_file_path );
      load_files(input_file_path, markdown_strings);
    } else if ( extentions.includes(ext) ){
      console.log('loading: '+ input_file_path );
      var page_name = input_file_path.split(/\/|\./).slice(-2)[0];
      var location = input_file_path.split(/\/|\./).slice(2,-2);
      let file_string = fs.readFileSync(input_file_path, {encoding: 'utf8'});
      var page_id;
      if( location.length > 0 ){
        page_id = location.join('/') + '/' + page_name;
      } else {
        page_id = page_name;
      }
      markdown_strings[page_id] = file_string;
    }
  });
  return markdown_strings;
}
let markdown_strings = load_files(config.markdown_input_path);
let pages = markdown_to_specdom(markdown_strings);


//npx dynatic fix_link_path src/client -a 'assets/' ",
Object.keys(pages).forEach( page_name => {
  console.log('fixing links for:',page_name);
  let page_specs = pages[page_name];
  let page_vars = page_specs.meta || {};
  page_vars.presentation_href = '/presentation/' + page_vars.page_id;
  if ( page_specs.meta && page_specs.meta.page_name && ! ['intro', 'home', '404'].includes( page_specs.meta.page_name ) ){
    page_specs.children.push({
      tag: 'div',
      class: 'footer',
      children: [
        {
          tag: 'span',
          children: [
            {
              tag: 'a',
              text: 'view presentation',
              meta: {
                var_id: 'page_id',
                var_type: 'variable',
                var_location: 'props.href',
                var_template: [ '/presentation/', 'page_id' ] ,
              },
            },
          ]
        },
      ],
    });
  }
  let path = './' + ['#'].concat( page_name.split('/').slice(0,-1) ).join('/') + '/';
  page_specs = fix_link_path(page_specs,{path});
  page_specs = compact(page_specs);
});
console.log('preparing presentation dir...');
fse.emptyDirSync('./src/public/presentation');
console.log('presentify...');
presentify(config.presentation_static_path, config.markdown_input_path, config.presentation_output_path, config.public_path);
console.log('preparing public...');
fse.emptyDirSync('public');
fse.copySync('./src/public', './public');
fse.copySync('./src/pages/assets', './public/assets');
console.log('saving json');
let output_json_path = path.join(config.json_output_path, 'pages.json');
let pages_json = JSON.stringify(pages);
console.log('into: '+ output_json_path );
fs.writeFileSync(output_json_path, pages_json);
console.log('Starting building webpack app.js' );
compiler.run( (err, res) => {
  if (err) {
    console.log(err);
  } else {
    console.log('Webpacked.');
  }
});

