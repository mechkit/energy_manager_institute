# Notes

Image URL example:
    http://10.173.64.137/wp_Sensei/wp-content/uploads/PV/images/PV_fundamentals_Solar_Iradiance.png

Browser slides
    https://github.com/impress/impress.js

Convert markdown to OPD slides
    https://github.com/thorstenb/odpdown

    tutorial:
    https://blog.thebehrens.net/2015/03/13/announcing-odpdown-a-markdown-to-odp-converter/

Convert OPD slides to powerpoint

    somthing like:
    libreoffice --headless --convert-to pdf:calc_pdf_Export --outdir pdf/ template.xlsx
    libreoffice --headless --convert-to ppt:????? --outdir pptx/ template.pptx