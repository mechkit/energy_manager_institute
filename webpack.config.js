var path = require('path');
var webpack = require('webpack');
//var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: ['./src/client/app.js'],
    presentation: ['./src/client/presentation.js'],
  },
  output: {
    //path: path.resolve(__dirname, 'public'),
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: '[name].js'
  },

  //plugins: [
  //  new webpack.DefinePlugin({
  //    'process.env.NODE_ENV': JSON.stringify('dev')
  //  })
  //],
  devtool: 'source-map',

  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 8008
  },
  watchOptions: {
    ignored: [
      /node_modules([\\]+|\/)+(?!markdown_loader)/,
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: /client/,
        //exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
      },
    ]

  }

};
